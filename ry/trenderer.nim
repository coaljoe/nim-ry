import strutils, glm, opengl, fp/option, ecs
import ry_types, globals
#proc render*(tr: TRenderer, r: Renderer)
import renderer, texture, card, rendertarget, node, nodeex, transform, scene, camera,
  texture, texturesys, framebuffer, resourcesys, mesh, material, gbuffer, shader, light, vars

proc newTRendererTile*(tsize: int): TRendererTile =
  let tex = CreateTexture(tsize, tsize, false)
  let card = newCard()
  card.setDim(0.5, 0.5, 0.5, 0.5)
  card.tex = tex
  #card.is2d = false
  #[
  let mn = gResourcesys.loadMesh("res/primitives/box_textured/box_textured.obj")
  let m = mn.get(Mesh)
  #let mn = gResourcesys.loadMesh("res/primitives/square_textured/square_textured.obj")
  let defaultMat = newMaterial("res/materials/model.json")
  defaultMat.name = "defaultMat"
  defaultMat.diffuse = vec3f(0.2, 0.5, 0.2)
  defaultMat.setTexture("res/primitives/box_textured/texture.png")
  m.setMaterial(defaultMat)
  let mat = m.material
  mat.texture = tex
  gScene.addNode(mn)
  ]#
  
  let fbo = newFramebuffer(tsize, tsize, false)
  #let fbo = newFramebuffer(vars.resX-1, vars.resY-1, false)
  #let fbo = newFramebuffer(vars.resX div 2, vars.resY div 2, false)
  let gb = newGBuffer(tsize, tsize)
  #fbo.clearColor(0, 1, 0, 1)
  gb.fbo.clearColor(0, 1, 0, 1)
  #card.tex.tex = fbo.tex
  #card.tex.tex = gb.colorTex.tex
  let rt = TRendererTile(tex: tex, card: card, gb: gb, fbo: fbo, mesh: nil, dirty: true)
  #let rt = TRendererTile(tex: tex, card: card, fbo: fbo, mesh: nil, dirty: true)
  #let rt = TRendererTile(tex: nil, card: card, dirty: true)
  gRenderer.renderables.add(asRenderableI(rt.card))
  rt
  
#proc fbo*(t: TRendererTile): Framebuffer =
#  t.gb.fbo


#############
## TRenderer

# Forward decl.
proc spawn(tr: TRenderer)

proc newTRenderer*(width, height: int): TRenderer =
  let tsize = 256
  let tr = TRenderer(
    width: width,
    height: height,
    tsize: tsize,
    tiles: @[],
    nodes: @[],
    rt: newRenderTarget("TRenderer RT", vars.resX, vars.resY, false),
    #tileRt: newRenderTarget("TRenderer tile RT", tsize, tsize, false),
    tcam: newCameraNode(name = "tcam node"),
    visible: true)
  # Set camera position
  tr.tcam.get(Transform).pos = vec3f(0, 0, 10)
  #tr.tcam.get(Transform).pos = vec3f(20, 20, 10)
  #tr.tcam.get(Camera).setupView(70, float(vars.resX)/float(vars.resY),  0.1, 1000, true)
  #tr.tcam.get(Camera).setupView(45, float(vars.resX)/float(vars.resY),  0.1, 1000, true)
  tr.tcam.get(Camera).setupView(45, 1.0,  0.1, 1000, true)
  gScene.addNode(tr.tcam)
  #tr.tcamInitialPoint = gScene.camera.transform.pos
  tr.tcamInitialPos = None[Vec3f]()
  tr.spawn()
  #quit 2
  tr

proc spawn(tr: TRenderer) =
  echo "TRenderer: spawn"
  # Spawn
  for y in 0..<tr.height:
    for x in 0..<tr.width:
      let t = newTRendererTile(tr.tsize)
      #let pw = vars.pw
      #let ph = vars.ph
      #t.card.setDim(x * Renderer.getPw(), y * Renderer.getPh(), 0.1f, 0.1f)
      #t.card.setDim(float(x*tr.tsize) * pw, float(y*tr.tsize) * ph,
      #  float(tr.tsize) * pw, float(tr.tsize) * ph)
      tr.tiles.add(t)
      
proc addNode*(tr: TRenderer, n: Node) =
  echo "TRenderer: addNode: ", n
  tr.nodes.add(n)
  n.visible = false
  
proc removeNode*(tr: TRenderer, n: Node) =
  echo "TRenderer: removeNode: ", n
  let idx = tr.nodes.find(n)
  if idx != -1:
    tr.nodes.delete(idx)
  else:
    quit "node not found"
  
proc showNodes*(tr: TRenderer) =
  echo "TRenderer: showNodes"
  echo tr.nodes
  #echo repr tr.nodes
  for n in tr.nodes:
    n.visible = true
    echo "showing node: ", n.id
  #echo repr tr.nodes
    
proc hideNodes*(tr: TRenderer) =
  echo "TRenderer: hideNodes"
  echo tr.nodes
  for n in tr.nodes:
    n.visible = false
    
proc refresh*(tr: TRenderer) =
  echo "TRenderer: refresh"
  for t in tr.tiles:
    t.dirty = true
    
proc hide*(tr: TRenderer) =
  tr.visible = false
  #for t in tr.tiles:
  #  t.visible = false

proc show*(tr: TRenderer) =
  tr.visible = true
  #for t in tr.tiles:
  #  t.visible = true

proc render*(tr: TRenderer, r: Renderer) =
  if not tr.visible:
    return
  let cam_fov = gScene.camera.get(Camera).fov
  #echo cam_fov
  #quit 2
  #echo gScene.camera.get(Camera).ortho_planes[0]
  #echo gScene.camera.get(Camera).ortho_planes[1]
  #echo gScene.camera.get(Camera).ortho_planes[2]
  #echo gScene.camera.get(Camera).ortho_planes[3]
  #let res_frac = float(vars.resX) / float(tr.tsize)
  let res_frac = float(tr.tsize) / float(vars.resX)
  let tcam_fov = cam_fov * res_frac
  #tr.tcam.get(Camera).fov = cam_fov / 8
  tr.tcam.get(Camera).fov = tcam_fov
  tr.tcam.get(Camera).build()
  #echo tr.tcam.get(Transform).pos
  let c_point = gScene.camera.get(Transform).pos
  let c_rot = gScene.camera.transform.rot
  echo c_point
  echo c_point / res_frac

  #tr.tcam.get(Transform).pos = c_point
  #tr.tcam.transform().pos = c_point
  #tr.tcam.transform().pos = vec3f(0, 0, 10)
  #tr.tcam.transform().pos = vec3f(0, 0, 0)
  tr.tcam.transform().pos = tr.tcamInitialPos.get
  tr.tcam.transform().rot = c_rot
  #tr.tcam.transform().translateLocal(vec3f(0, 0, 10))
  tr.tcam.transform.forceBuild()

  echo tr.tcam.transform().pos
  echo tr.tcam.transform()
  #quit 2

  if false:
    echo "Camera:"
    echo repr(tr.tcam.get(Camera))
    echo tr.tcam.get(Camera).getProjectionMatrix()
    echo tr.tcam.get(Camera).getViewMatrix()
    echo "gCamera:"
    echo repr(gScene.camera.get(Camera))
    echo gScene.camera.get(Camera).getProjectionMatrix()
    echo gScene.camera.get(Camera).getViewMatrix()
    quit 2
  
  #r.unsetCamera()

  block:
    let pw = vars.pw
    let ph = vars.ph
    for y in 0..<tr.height:
      for x in 0..<tr.width:
        let tile = tr.tiles[y*tr.height+x]
        #tile.card.setDim(
        #  #((c_point.x) + (float(x*tr.tsize)) * pw), float(y*tr.tsize) * ph,
        #  -(c_point.x / res_frac * pw), float(y*tr.tsize) * ph,
        #  float(tr.tsize) * pw, float(tr.tsize) * ph)
        #tile.card.setDim(float(x*tr.tsize) * pw, float(y*tr.tsize) * ph,
        #  float(tr.tsize) * pw, float(tr.tsize) * ph)
        let tsizeuX = float(tr.tsize)*pw
        let tsizeuY = float(tr.tsize)*ph
        tile.card.setDim((0.5 - tsizeuX/2) + (-vars.shX*pw) + (float(x*tr.tsize) * pw),
          (0.5 - tsizeuY/2) + (-vars.shY*ph) + (float(y*tr.tsize) * ph),
          float(tr.tsize) * pw, float(tr.tsize) * ph)
        #[
        #tile.mesh.transform.pos = vec3f(float(x) * 2, 1, 0)
        tile.mesh.transform.pos = vec3f(0, 1, 0)
        tile.mesh.transform.translateLocal(vec3f(float(x) * 1, 0, 0))
        tile.mesh.transform.rot = vec3f(50, 0, 45)
        #tile.mesh.transform.translate(vec3f(10, 0, 0))
        tile.mesh.transform.scale = vec3f(4, 4, 0)
        #tile.mesh.transform.lookAt(c_point)
        #tile.mesh.transform.lookAt(tile.mesh.transform.pos + c_rot * vec3f(1, 0, 0),
        #  up = c_rot * vec3f(0,0,1))
        #tile.mesh.transform.billboard(c_point)
        #tile.mesh.transform.pos = vec3f(3, 3, 0)
        ]#


        
  #tr.showNodes()
  
    
  for y in 0..<tr.height:
    for x in 0..<tr.width:
      let tile = tr.tiles[y*tr.height+x]

      var updateGbufferGeometry = true
      var updateGbufferLighting = true
      
      if not tile.dirty:
        updateGbufferGeometry = false
        #continue
        discard

      ## Prepare
      
      let yshift = float(y) * tcam_fov
      let xshift = float(x) * tcam_fov
      
      if x == 0 and y == 0:
        discard
      else:
        tr.tcam.transform.pos = tr.tcamInitialPos.get
        tr.tcam.transform.translateLocal(vec3f(xshift, yshift, 0))
      
      r.setViewportSize(tr.tsize, tr.tsize)
      r.setCamera(tr.tcam)
      

      ##
      ## Geometry pass
      ##

      if updateGbufferGeometry:

        tr.showNodes() 
        #glPushMatrix()
        #glLoadIdentity()
        #tile.tex.gl_tex = tr.rt.tex()
        #r.finalize()
        #tr.rt.bindd()
        #r.reset()
        #tile.fbo.bindd()
        tile.gb.bindForWriting()
        r.clear()
        r.reset()
        if x == 0 and y == 0:
          r.clearColor(1, 0, 0, 1)
          discard
        else:
          r.clearColor(0.5, 0, 0, 0.2)
        #r.renderDebug()
        #r.renderScene()
        # Update gbuffer
        r.renderGBufferGeometryPass()
        #CopyTexture2(tile.tex.tex, 0, 0, tr.tsize, tr.tsize)
        #gTextureSys.copyTex(tr.rt.tex(), tile.tex.tex)
        #tile.tex.tex = tr.rt.tex()
        #tile.card.tex.tex = tile.fbo.tex
        tile.card.tex.tex = tile.gb.colorTex.tex
        #tile.card.tex.tex = tile.gb.positionTex.tex
        #tile.card.tex.tex = tile.gb.normalTex.tex
        #tile.tex.tex = tile.fbo.tex
        #tr.rt.unbind()
        #tile.fbo.unbind()
        tile.gb.unbind()

        tr.hideNodes()

        tile.dirty = false

      ##
      ## Lighting pass
      ##   

      if updateGbufferLighting:

        tile.fbo.bindd()
  
        r.lightingShader.bindd()

        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE2D, tile.gb.colorTex.tex)
        r.lightingShader.passUniform("tDiffuse", 0, check = true)
  
        glActiveTexture(GL_TEXTURE1)
        glBindTexture(GL_TEXTURE2D, tile.gb.positionTex.tex)
        r.lightingShader.passUniform("tPosition", 1, check = true)
    
        glActiveTexture(GL_TEXTURE2)
        glBindTexture(GL_TEXTURE2D, tile.gb.normalTex.tex)
        r.lightingShader.passUniform("tNormals", 2, check = true)
    
        r.lightingShader.passUniform("cameraPosition", r.cam.transform.pos, check = true)
        #r.lightingShader.passUniform("lightPosition", r.scene.light0.transform.pos, check = true)
        let lightNodes = r.scene.getNodesWithComponent(Light)
        for i, n in lightNodes:
          let lc = n.get(Light)
          r.lightingShader.passUniform("lights[$1].position".format(i), n.transform.pos, check = true)
          r.lightingShader.passUniform("lights[$1].color".format(i), lc.color, check = true)

        r.renderFsQuad()
    
        r.lightingShader.unbind()

        tile.fbo.unbind()

        tile.card.tex.tex = tile.fbo.tex

      
      #tile.card.tex.tex = tile.fbo.tex
      #gTextureSys.copyTexture(tr.rt.tex(), tile.card.tex.tex)
      #CopyTexture2(tile.tex.tex, 0, 0, tr.tsize, tr.tsize)
      #r.finalize()
      #glPopMatrix()
      r.unsetCamera()
      #r.setCamera(r.cam)
      #r.setViewportSize(vars.resX, vars.resY)
      r.resetViewportSize()
      
  #tr.hideNodes()
  r.resetViewportSize()
  r.unsetCamera()
  r.setCamera(r.cam)
  