import os, strutils, opengl, stb_image/read as stbi
import ry_types
import glex, vars
import texturesys

let
  TEXTURE_MAX_ANISOTROPY_EXT     = 0x84FE
  MAX_TEXTURE_MAX_ANISOTROPY_EXT = 0x84FF
  
# Forward decl.
#proc load(t: Texture, path: string): Texture
proc loadFromFile(path: string): Texture
  
proc newTexture*(path: string): Texture =
  #[
  let t = Texture(
    format:     TextureFormat.RGBA,
    minFilter:  TextureFilter.Linear, # default
    magFilter:  TextureFilter.Linear, # default
    #mipmaps:    !_opts[0].Nomipmaps,
    #compress:   false,
    #anisotropy: true
    gl_min_filter: GL_LINEAR,
    gl_mag_filter: GL_LINEAR)
  ]#
  #let t = CreateTexture()
  #let t = Texture()
  result = loadFromFile(vars.resPath & path)
  glcheck()
  #t

proc bindd*(t: Texture) =
    glActiveTexture(GL_TEXTURE0) # fixme: remove?
    glBindTexture(GL_TEXTURE_2D, t.tex)

proc unbind*(t: Texture) =
    glBindTexture(GL_TEXTURE_2D, 0)

proc build(t: Texture) =
  t.bindd() # warning: dangerous, can rebind existing texture bind. (fixme?)

  var
    minf: GLint
    magf: GLint

  case t.minFilter
  of TextureFilter.Nearest:
    minf = GL_NEAREST
  of TextureFilter.Linear:
    minf = GL_LINEAR
  else:
    quit("unknown filter")

  case t.magFilter
  of TextureFilter.Nearest:
    magf = GL_NEAREST
  of TextureFilter.Linear:
    magf = GL_LINEAR
  else:
    quit("unknown filter")

  t.gl_min_filter = minf
  t.gl_mag_filter = magf

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, t.gl_min_filter)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, t.gl_mag_filter)
  #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
  #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
  glcheck()

  ## update width, height
  #var w, h int32
  #glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &w)
  #glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &h)
  #t.width, t.height = int(w), int(h)

  t.unbind() # warning: dangrous (can unbind existing GL_TEXTURE_2D)
  
proc size*(t: Texture): (int, int) =
  (t.width, t.height)

proc free*(t: Texture) =
  echo("Texture.Free")
  glDeleteTextures(1, addr t.tex)

proc CreateTexture*(width, height: int, depth: bool = false, format = TextureFormat.RGBA,
  filter = [TextureFilter.Linear, TextureFilter.Linear]): Texture =

  # create texture record (fixme?)
  var t = Texture(
    width:      width,
    height:     height,
    #format:     TextureFormatRGBA,
    minFilter:  TextureFilter.Linear,
    magFilter:  TextureFilter.Linear,
    #mipmaps:    false,
    #compress:   false,
    #anisotropy: false,
    #tex:        tex,
    #gl_min_filter: GL_LINEAR,
    #gl_mag_filter: GL_LINEAR
  )
  
  var tex: GLuint
  glGenTextures(1, addr tex)
  t.tex = tex
  #tex.Bind(GL_TEXTURE_2D)
  glBindTexture(GL_TEXTURE_2D, tex)

  #[
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
  #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
  #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
  ]#

  if not depth:
    case format
    of TextureFormat.RGBA:
      glTexImage2D(GL_TEXTURE_2D, 0, GLint(GL_RGBA8),
        GLsizei(width), GLsizei(height), 0, GL_RGBA, cGL_UNSIGNED_BYTE, nil)
    of TextureFormat.RGB16F:
      glTexImage2D(GL_TEXTURE_2D, 0, GLint(GL_RGB16F),
        GLsizei(width), GLsizei(height), 0, GL_RGB, cGL_UNSIGNED_BYTE, nil)
    of TextureFormat.RGB32F:
      glTexImage2D(GL_TEXTURE_2D, 0, GLint(GL_RGB32F),
        GLsizei(width), GLsizei(height), 0, GL_RGB, cGL_UNSIGNED_BYTE, nil)
    else:
      echo "unknown format: ", format
      quit -1
  else:
    glTexImage2D(GL_TEXTURE_2D, 0, GLint(GL_DEPTH_COMPONENT24),
      GLsizei(width), GLsizei(height), 0, GL_DEPTH_COMPONENT, cGL_FLOAT, nil)

    #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL)
    #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE)
  
  #tex.Unbind(GL_TEXTURE_2D)
  t.build()
  glBindTexture(GL_TEXTURE_2D, 0)
  t

proc loadFromFile(path: string): Texture =
  echo "loading image; path: ", path
  #let imData = t.loadImageData(path)
  #let data_i, ix, iy := imData.Data, imData.W, imData.H
  if not os.existsFile(path):
    quit("path not found; path: " & path)
  var
    ix, iy, channels: int
    data_i: seq[uint8]
  #data_i = stbi.load(path, ix, iy, channels, stbi.Default)
  #data_i = stbi.load(path, ix, iy, channels, 4)
  data_i = stbi.load(path, ix, iy, channels, 3)
  #data_i = stbi.load(path, ix, iy, channels, 4)
  #quit("channels: $1".format(channels))

  let t = CreateTexture(ix, iy)

  #glGenTextures(1, addr t.tex)
  glBindTexture(GL_TEXTURE_2D, t.tex)
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1)

  ## XXX duplicate: remove?
  #      if t.mipmaps {
  #          glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE)
  #          //  t.min_filter = GL_LINEAR_MIPMAP_LINEAR
  #      }

  #let image_data = data_i
  # Use RGBA (32bit) from all textures
  #glTexImage2D(GL_TEXTURE_2D, 0, GLint(GL_RGBA8), GLsizei(ix), GLsizei(iy), 
  #  0, GL_RGBA, cGL_UNSIGNED_BYTE, addr data_i[0])
  glTexImage2D(GL_TEXTURE_2D, 0, GLint(GL_RGB8), GLsizei(ix), GLsizei(iy), 
    0, GL_RGB, cGL_UNSIGNED_BYTE, addr data_i[0])

  #glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GLint(GL_TRUE)) 
  #glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GLint(1)) 
  #t.min_filter = GL_LINEAR_MIPMAP_LINEAR
  t.build()

  glBindTexture(GL_TEXTURE_2D, 0)
  t

proc Copy(t: Texture, dst: Texture) =
  gTextureSys.copyTexture(t, dst)

#proc CopyTexture*(src, dst: uint32, width, height: int) =
proc CopyTexture*(dst: uint32, width, height: int) =
  ## Copies pixels from active screen (GL_READ_BUFFER) to dst texture.
  ## XXX not properly tested.
  glBindTexture(GL_TEXTURE_2D, dst)
  glCopyTexSubImage2d(GL_TEXTURE_2D, 0, 0, 0, 0, 0, GLint(width), GLint(height))
  glBindTexture(GL_TEXTURE_2D, 0)

proc CopyTexture2*(dst: uint32, x, y, width, height: int) =
  ## Copies pixels from active screen (GL_READ_BUFFER) to dst texture.
  ## XXX not properly tested.
  
  var prevTex: GLint
  glGetIntegerv(GL_TEXTURE_BINDING_2D, prevTex.addr)

  glBindTexture(GL_TEXTURE_2D, dst)
  glCopyTexSubImage2d(GL_TEXTURE_2D, 0, 0, 0, GLint(x), GLint(y), GLint(width), GLint(height))
  glBindTexture(GL_TEXTURE_2D, 0)

  if prevTex != 0:
    echo("restoring prevTex")
    glBindTexture(GL_TEXTURE_2D, GLuint(prevTex))
