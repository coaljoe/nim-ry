import glm
import ry_types
import glmex, texture, shader, vars
#export shader, texture

var
  maxMaterialId = -1

type
  Material* = ref object
    id*: int
    name*: string
    diffuse*: Vec3f
    specular*: Vec3f
    ambient*: Vec3f
    emission*: Vec3f
    hardness*: float
    alpha*: bool
    texture*: Texture
    shader*: Shader

# Forward decl.
#proc load(m: Material, path: string)

proc newMaterial*(path: string): Material =
  maxMaterialId += 1
  var m = Material(
    id: maxMaterialId,
    name: "",
    diffuse: Vec3One,
    specular: vec3f(0.5),
    ambient: vec3f(0.1),
    hardness: 50.0)
  echo "[Material] New material, id=", m.id
  #m.load(path)
  #m.shader.compile()

  let
    vs_path = "shaders/model.vert"
    fs_path = "shaders/model.frag"
    #vs_path = "shaders/static.vert"
    #fs_path = "shaders/static.frag"
  m.shader = newShader(vs_path, fs_path)
  m.shader.compile()

  if m.shader == nil:
    quit "shader is nil"
  m


proc load(m: Material, path: string) =
  discard #[
  d := getJsonFromFile(path)

  m.name = d.Get("name").MustString()
  vs_path := d.Get("shaders").GetIndex(0).MustString()
  fs_path := d.Get("shaders").GetIndex(1).MustString()
  ]#

  let
    vs_path = "shaders/model.vert"
    fs_path = "shaders/model.frag"
    #vs_path = "shaders/static.vert"
    #fs_path = "shaders/static.frag"

  m.shader = newShader(vs_path, fs_path)
  #echo m.shader, isNil(m.shader)
  #quit 2
  #m.shader.compile()


proc setTexture*(m: Material, path: string) =
  let tex = newTexture(path)
  m.texture = tex
  m.shader.vs_macro &= "#define _TEXTURE_MAP 1\n"
  m.shader.fs_macro &= "#define _TEXTURE_MAP 1\n"
  m.texture.bindd()
  m.shader.compile()
  m.texture.unbind()

