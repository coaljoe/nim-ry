import ry_types, app, renderer, transform, glmex, scene, sceneex, resourcesys, spritemeshsys, texturesys
import vars as vars
export ry_types, transform, glmex, resourcesys

type
  Ry* = ref object
    #win: Window
    app*: App
    renderer*: Renderer
    resourcesys*: ResourceSys
    ## Context
    scene*: Scene
    # XXX Not context?
    #spriteMeshSys*: SpriteMeshSys

#proc ryi*: Ry =
#  return 0

proc newRy*(): Ry =
  var ry: Ry
  ry = Ry(
    app: newApp(),
    renderer: newRenderer(),
    resourceSys: newResourceSys(),
    scene: newScene())
  ry


proc init*(r: Ry, width: int, height: int) =
  # Update vars
  vars.resX = width
  vars.resY = height

  r.app.init(width, height)
  r.scene.setDefaultScene()
  r.renderer.init(width, height, r.scene)
  discard initSpriteMeshSys()
  initTextureSys()

  
proc render*(r: Ry) =
  r.renderer.render()


proc step*(r: Ry): bool =
  let ret = r.app.step()
  let dt = r.app.dt
  #gSpriteMeshSys.update(dt)
  r.render() # XXX move to app.step?
  ret

  
proc dt*(r: Ry): float =
  return r.app.dt
