#import interfaced
import collections/iface, strutils
import opengl, glm, glu, ecs
import ry_types, globals
import transform, node, camera, scene, mesh, light, texture, draw, rendertarget, renderpass, gbuffer, vars
import shader, material


# Forward decl.
proc setViewportSize*(r: Renderer, w, h: int)
proc resetViewportSize*(r: Renderer)


proc newRenderer*(): Renderer =
  result = Renderer(
    renderPasses: @[],
    renderables: @[],
    m_typ: RenderType.Forward,
    debug: true)
  gRenderer = result
  #renderables: newSeq[Renderable](),


proc init*(r: Renderer, width: int, height: int, scene: Scene) =
  r.width = width
  r.height = height
  r.scene = scene
  r.cam = scene.camera # XXX fixme?
  r.tmpRt = newRenderTarget("TmpRT", r.width, r.height, false)
  
  #glClearColor(0.0, 0.0, 0.0, 1.0)
  #glClearColor(0.0, 0.0, 0.0, 0.0) # XXX warning, transparent background
  glClearDepth(1.0)
  glColor3f(1.0, 1.0, 1.0)
  glEnable(GL_DEPTH_TEST)
  glDepthFunc(GL_LEQUAL)
  
  glEnable(GL_TEXTURE_2D)
  glEnable(GL_CULL_FACE)
  
  # Set blend func
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
  #glEnable(GL_BLEND) # XXX should be disabled by default
  
  # Set viewport
  #glViewport(0, 0, GLsizei(r.width), GLsizei(r.height))
  #r.setViewportSize(r.width, r.height)
  r.resetViewportSize()

  # Default projection
  if true:
    # set projection matrix
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
  
    # Set perspective by default XXX fixme: remove
    # Enable perspective projection with fovy, aspect, zNear and zFar
    gluPerspective(45.0, float(width) / float(height), 0.1, 100.0)
  
  # Set modelview matrix
  glMatrixMode(GL_MODELVIEW)
  glLoadIdentity()

  # Update vars
  vars.pw = 1.0 / float(r.width)
  vars.ph = 1.0 / float(r.height)
  

proc setRenderType*(r: Renderer, typ: RenderType) =
  r.m_typ = typ
  if r.m_typ == RenderType.Deferred:
    r.gbuffer = newGbuffer(r.width, r.height)
    r.geometryShader = newShader("shaders/deferredGeometry.vert", "shaders/deferredGeometry.frag",
      version = 120)
    r.lightingShader = newShader("shaders/deferredLighting.vert", "shaders/deferredLighting.frag",
      version = 120)

      
proc reset*(r: Renderer) =
  glMatrixMode(GL_MODELVIEW)
  glLoadIdentity()
  #glColor3f(0.0, 1.0, 0.0)     # Green
  # XXX warning, transparent background
  #glClearColor(0.0, 0.0, 0.0, 0.0)
  #glClearColor(0.2, 0.3, 0.8, 1.0)
  glClearColor(0.0, 0.0, 0.0, 1.0)
  # Set viewport
  #glViewport(0, 0, GLsizei(r.width), GLsizei(r.height))


proc finalize*(r: Renderer) =
  glActiveTexture(GL_TEXTURE0)
  glBindTexture(GL_TEXTURE_2D, 0)
  # XXX reset viewport size
  #r.setViewportSize(r.width, r.height)

  
proc clear*(r: Renderer) =
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)


proc clearColor*(r: Renderer, xr, g, b, a: float) =
  ## Clear screen with color
  glClearColor(xr, g, b, a)
  glClear(GL_COLOR_BUFFER_BIT)
  r.reset()


proc setViewportSize*(r: Renderer, w, h: int) =
  echo "Renderer: setViewportSize: ", w, " ", h
  glViewport(0, 0, GLsizei(w), GLsizei(h))


proc resetViewportSize*(r: Renderer) =
  ## Can be done via glPushAttrib(GL_VIEWPORT_BIT)
  echo "Renderer: resetViewportSize"
  glViewport(0, 0, GLsizei(r.width), GLsizei(r.height))
  

proc enableBlending*(r: Renderer) =
  echo "[Renderer] enable blending"
  glEnable(GL_BLEND)


proc disableBlending*(r: Renderer) =
  echo "[Renderer] disable blending"
  glDisable(GL_BLEND)


proc setCamera*(r: Renderer, n: Node) =
  let cam = n.get(Camera)
  # Set projection matrix
  glMatrixMode(GL_PROJECTION)
  glLoadIdentity()
  
  var projM = Mat4x4[float32](cam.getProjectionMatrix())
  glMultMatrixf(projM.caddr) # Multiply by projection matrix
  #glMultMatrixf(projM[0].caddr) # Multiply by projection matrix

  let viewM = cam.getViewMatrix()
  #echo cam.getProjectionMatrix()
  #echo viewM
  #quit 2
  var m = Mat4x4[float32](viewM)
  let d = m.caddr
  #let d = m[0].caddr
  glMultMatrixf(d) # Multiply by view matrix
  
  # Set model matrix
  glMatrixMode(GL_MODELVIEW)
  glLoadIdentity()


proc unsetCamera*(r: Renderer) =
  # Set projection matrix
  glMatrixMode(GL_PROJECTION)
  glLoadIdentity()

  # Set model matrix
  glMatrixMode(GL_MODELVIEW)
  glLoadIdentity()


proc setMaterial(r: Renderer, mat: Material) =
  #echo "[Renderer] setMaterial"
  #defer: echo "[Renderer] setMaterial done"
  #echo "-> mat: ", "isNil: ", isNil(mat)
  #echo "-> mat: ", mat.id
  #echo "-> mat: ", mat.name
  #echo "-> shader: ", "isNil: ", isNil(mat.shader)
  #echo "-> shader: ", mat.shader.id
  #quit 2
  #let shader = mat.shader
  #shader.`bind`()
  
  #shader.`bind`()
  mat.shader.bindd()
  #shader.enableUniformsCheck() # TODO: add this?

  var shader = mat.shader
  shader.passUniform("mat.diffuse", mat.diffuse)
  shader.passUniform("mat.specular", mat.specular)
  shader.passUniform("mat.hardness", mat.hardness)
  #quit 2

  let l0 = r.scene.light0
  if l0 != nil:
    shader.passUniform("lightPos", l0.get(Transform).pos, check = true)
    #shader.passUniform("light.intensity", l0.get(Light).intensity, check = true)
  else:
    quit("no light in the scene")

  # Texture
  if mat.texture != nil:
    glActiveTexture(GL_TEXTURE0)
    mat.texture.bindd()
    shader.passUniform("texMap", 0)

  # Alpha
  if mat.alpha:
    r.enableBlending()
  #  glEnable(GL_BLEND)
  #  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
  #  glDepthMask(GL_FALSE)


proc unsetMaterial(r: Renderer, mat: Material) =
  #echo "[Renderer] unsetMaterial"
  #defer: echo "[Renderer] unsetMaterial done"
  mat.shader.unbind()
  if mat.texture != nil:
    mat.texture.unbind()
  
  if mat.alpha:
    r.disableBlending()
  #  glDisable(GL_ALPHA_TEST)
  #  glDisable(GL_BLEND)
  #  glDepthMask(GL_TRUE)


proc renderMesh*(r: Renderer, n: Node) =
  echo "[Renderer] renderMesh"
  #defer: echo "[Renderer] renderMesh done"
  let m = n.get(Mesh)
  echo "-> ", m.meshName
  echo "-> ", m.id
  echo "-> ", m.hasMaterial()
  echo "-> ", n.get(Transform).pos
  #if m.id > 2:
  #  quit 2

  #if m.material != nil:
  #if m.hasMaterial():
  #  echo "material.name: ", m.material.name

  # Set material
  case r.m_typ
  of RenderType.Forward:
    if m.hasMaterial():
      #echo "shader is nil: ", isNil(m.material.shader)
      #echo "shader id: ", m.material.shader.id
      #echo repr(m.material.shader)
      #quit 2
      #discard
      r.setMaterial(m.material)
  of RenderType.Deferred:
    # Set only diffuse texture in deferrend
    let mat = m.material
    if mat.texture != nil:
      mat.texture.bindd()
  else:
    quit "unknown render type"
    

  glPushMatrix()
  var mat = Mat4x4[float32](n.get(Transform).mat)
  var d = mat.caddr
  glMultMatrixf(d)
  #glLoadIdentity()
  m.mb.render()
  glPopMatrix()
  #echo m.mb.positions
  #quit 2

  # Unset material
  case r.m_typ
  of RenderType.Forward:
    if m.hasMaterial():
      #discard
      r.unsetMaterial(m.material)
  of RenderType.Deferred:
    let mat = m.material
    if mat.texture != nil:
      mat.texture.unbind()
  else:
    quit "unknown render type"
      
  #r.geometryShader.bindd()


proc renderRenderable*(r: Renderer, ri: RenderableI) =
  discard ri.render(r)


proc renderScene*(r: Renderer) =
  for n in r.scene.getNodes:
    if not n.visible:
      continue
    case n.nodetype:
      of NodeType.Mesh: # XXX use mesh component check?
        r.renderMesh(n)
      else:
        discard


proc renderRenderables*(r: Renderer) =
  for ri in r.renderables:
    #discard ri.render(r)
    r.renderRenderable(ri)


proc renderDebug*(r: Renderer) =
  #echo "vars.debugRender: ", vars.debugRender
  if not vars.debugRender:
    discard
  else:
    #echo "* REACHED *"
    if r.debug:
      #echo "* drawGrid *"
      drawGrid(10, 1)
      #echo "* done *"


proc renderFsQuad*(r: Renderer) =
  # From:
  # https://www.opengl.org/wiki/Viewing_and_Transformations#How_do_I_draw_a_full-screen_quad.3F

  # Prepare
  glPushAttrib(GLbitfield(GL_DEPTH_TEST))
  glDisable(GL_DEPTH_TEST)
  glColor3f(1.0, 1.0, 1.0) # important

  var z: float32 = -1.0

  glMatrixMode(GL_MODELVIEW)
  glPushMatrix()
  glLoadIdentity()
  glMatrixMode(GL_PROJECTION)
  glPushMatrix()
  glLoadIdentity()
  glBegin(GL_QUADS)
  glTexCoord2i(0, 0)
  glVertex3f(-1, -1, z)
  glTexCoord2i(1, 0)
  glVertex3f(1, -1, z)
  glTexCoord2i(1, 1)
  glVertex3f(1, 1, z)
  glTexCoord2i(0, 1)
  glVertex3f(-1, 1, z)
  glEnd()
  glPopMatrix()
  glMatrixMode(GL_MODELVIEW)
  glPopMatrix()

  glPopAttrib()


proc renderQuad*(r: Renderer, x, y, w, h, z: float) =
  if r.debug:
    if w > 1.0 or h > 1.0:
      quit("w or h is too big (must be <= 1.0)")

  # Prepare
  glPushAttrib(GL_CURRENT_BIT or GL_TEXTURE_BIT or GLbitfield(GL_DEPTH_TEST))
  glDisable(GL_DEPTH_TEST)
  glColor3f(1, 1, 1) # important

  glMatrixMode(GL_PROJECTION)
  glPushMatrix()
  glLoadIdentity()
  glOrtho(0, 1, 0, 1, -1, 1) # normal coords
  #//gl.Ortho(0, 1, 1, 0, -1, 1) // Y-flipped
  glMatrixMode(GL_MODELVIEW)
  glPushMatrix()
  glLoadIdentity()

  #//gl.Translatef(0, 0, float32(z)) // ok
  #//gl.Translatef(0, float32(z), 0) // xyz upd: check [bad]

  glBegin(GL_QUADS)
  glTexCoord2f(0, 0)
  glVertex2f(x, y)
  glTexCoord2f(1, 0)
  glVertex2f(x+w, y)
  glTexCoord2f(1, 1)
  glVertex2f(x+w, y+h)
  glTexCoord2f(0, 1)
  glVertex2f(x, y+h)
  glEnd()

  # Restore
  glMatrixMode(GL_PROJECTION)
  glPopMatrix()
  glMatrixMode(GL_MODELVIEW)
  glPopMatrix()

  glPopAttrib()


proc set2DMode*(r: Renderer) =
  ## Enter 2D mode
  if r.in2dmode:
    return

  # Prepare
  glPushAttrib(GL_CURRENT_BIT or GL_TEXTURE_BIT or GLbitfield(GL_DEPTH_TEST))
  #gl.Disable(gl.DEPTH_TEST)
  #glDisable(gl.LIGHTING)
  glColor3f(1, 1, 1) # important

  glMatrixMode(GL_PROJECTION)
  glPushMatrix()
  glLoadIdentity()
  glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0) # normal coords
  #gl.Ortho(0, 1, 1, 0, -1, 1) // Y-flipped
  glMatrixMode(GL_MODELVIEW)
  glPushMatrix()
  glLoadIdentity()

  r.in2dmode = true


proc unset2DMode*(r: Renderer) =
  ## Exit 2D mode
  if not r.in2dmode:
      return
  
  #r.SetCamera(r.curCam)

  # Restore
  glMatrixMode(GL_PROJECTION)
  glPopMatrix()
  glMatrixMode(GL_MODELVIEW)
  glPopMatrix()

  glPopAttrib()

  r.in2dmode = false


proc addRenderPass*(r: Renderer, rp: RenderPassBase) =
  if r.renderPasses.contains(rp):
    quit("renderpass alread added; name: "&rp.name)
  r.renderPasses.add(rp)


proc removeRenderPass*(r: Renderer, rp: RenderPassBase) =
  quit("not implemented")


proc renderGBufferGeometryPass*(r: Renderer) =
  ## Execute GBuffers' geometry pass
  r.geometryShader.bindd()
  r.geometryShader.passUniform("WorldMatrix", r.cam.transform.mat())
  #r.gbuffer.bindForWriting()

  r.clear()
  r.reset()
  r.renderDebug()
  r.renderScene()
  
  #r.gbuffer.unbind()
  r.geometryShader.unbind()            
  

proc render*(r: Renderer) =
  echo "[Renderer] - render -"
  
  r.resetViewportSize()
  r.setCamera(r.cam)
  if r.m_typ == RenderType.Forward:
    r.tmpRt.bindd()
  elif r.m_typ == RenderType.Deferred:
    #quit "not implemented"
    r.gbuffer.bindForWriting()
    r.renderGBufferGeometryPass()
    r.gbuffer.unbind()
    #discard
  else:
    echo "unknown render type: ", r.m_typ
    quit -1

  r.clear()
  r.reset()
  #glMatrixMode(GL_MODELVIEW)
  #glLoadIdentity()
  #r.cam.get(Transform).pos = vec3f(1.5, 0.0, -10)
  #r.cam.get(Transform).pos = vec3f(0, 0.0, 500)
  #r.setCamera(r.cam)
  #r.setCamera(r.cam)
  #r.unsetCamera()
  #glTranslatef(1.5, 0.0, -10.0)
  #glTranslatef(1.5, 0.0, -1000.0)
  glColor3f(0.0, 1.0, 0.0)     # Green

  #[
  if false:
    r.setCamera(r.cam)

    if r.debug:
      drawGrid(100, 1)

    #r.renderScene()
  
  if false:
    glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT) # Clear color and depth buffers
    glMatrixMode(GL_MODELVIEW)                          # To operate on model-view matrix
    glLoadIdentity()                 # Reset the model-view matrix
    glTranslatef(1.5, 0.0, -7.0)     # Move right and into the screen

    # Render a cube consisting of 6 quads
    # Each quad consists of 2 triangles
    # Each triangle consists of 3 vertices

    glBegin(GL_TRIANGLES)        # Begin drawing of triangles

    # Top face (y = 1.0f)
    glColor3f(0.0, 1.0, 0.0)     # Green
    glVertex3f( 1.0, 1.0, -1.0)
    glVertex3f(-1.0, 1.0, -1.0)
    glVertex3f(-1.0, 1.0,  1.0)
    glVertex3f( 1.0, 1.0,  1.0)
    glVertex3f( 1.0, 1.0, -1.0)
    glVertex3f(-1.0, 1.0,  1.0)

    # Bottom face (y = -1.0f)
    glColor3f(1.0, 0.5, 0.0)     # Orange
    glVertex3f( 1.0, -1.0,  1.0)
    glVertex3f(-1.0, -1.0,  1.0)
    glVertex3f(-1.0, -1.0, -1.0)
    glVertex3f( 1.0, -1.0, -1.0)
    glVertex3f( 1.0, -1.0,  1.0)
    glVertex3f(-1.0, -1.0, -1.0)

    # Front face  (z = 1.0f)
    glColor3f(1.0, 0.0, 0.0)     # Red
    glVertex3f( 1.0,  1.0, 1.0)
    glVertex3f(-1.0,  1.0, 1.0)
    glVertex3f(-1.0, -1.0, 1.0)
    glVertex3f( 1.0, -1.0, 1.0)
    glVertex3f( 1.0,  1.0, 1.0)
    glVertex3f(-1.0, -1.0, 1.0)

    # Back face (z = -1.0f)
    glColor3f(1.0, 1.0, 0.0)     # Yellow
    glVertex3f( 1.0, -1.0, -1.0)
    glVertex3f(-1.0, -1.0, -1.0)
    glVertex3f(-1.0,  1.0, -1.0)
    glVertex3f( 1.0,  1.0, -1.0)
    glVertex3f( 1.0, -1.0, -1.0)
    glVertex3f(-1.0,  1.0, -1.0)

    # Left face (x = -1.0f)
    glColor3f(0.0, 0.0, 1.0)     # Blue
    glVertex3f(-1.0,  1.0,  1.0)
    glVertex3f(-1.0,  1.0, -1.0)
    glVertex3f(-1.0, -1.0, -1.0)
    glVertex3f(-1.0, -1.0,  1.0)
    glVertex3f(-1.0,  1.0,  1.0)
    glVertex3f(-1.0, -1.0, -1.0)

    # Right face (x = 1.0f)
    glColor3f(1.0, 0.0, 1.0)    # Magenta
    glVertex3f(1.0,  1.0, -1.0)
    glVertex3f(1.0,  1.0,  1.0)
    glVertex3f(1.0, -1.0,  1.0)
    glVertex3f(1.0, -1.0, -1.0)
    glVertex3f(1.0,  1.0, -1.0)
    glVertex3f(1.0, -1.0,  1.0)

    glEnd()  # End of drawing

  ]#


  r.renderDebug()
  r.renderScene()
  
  if r.m_typ == RenderType.Forward:
    r.renderRenderables()

  #if r.trenderer != nil:
  #  #r.trenderer.render(r)
  #  discard

  if r.m_typ == RenderType.Forward:
    r.tmpRt.unbind()

    # Render RT's texture on a fullscreen quad
    glBindTexture(GL_TEXTURE_2D, r.tmpRt.tex)
    r.renderFsQuad()
    glBindTexture(GL_TEXTURE_2D, 0)
  elif r.m_typ == RenderType.Deferred:
    # GBuffer Light pass
    #r.gbuffer.unbind()
    r.geometryShader.unbind()

    #if false:
    if r.showGbuffer:
 
      #glClearColor(0.0, 0.0, 0.0, 1.0)
      glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
 
      r.gbuffer.bindForReading()
 
      let halfW = GLint(r.width div 2)
      let halfH = GLint(r.height div 2)
      let w = GLint(r.width)
      let h = GLint(r.height)
 
      #r.gbuffer.setReadBuffer(0)
      glReadBuffer(GL_COLOR_ATTACHMENT0)
      glBlitFramebuffer(0, 0, w, h, 0, 0, halfW, halfH, GL_COLOR_BUFFER_BIT, GLenum(GL_LINEAR))
    
      r.gbuffer.setReadBuffer(1)
      #glReadBuffer(GL_COLOR_ATTACHMENT0)
      #glReadBuffer(GL_COLOR_ATTACHMENT1)
      #glReadBuffer(GL_COLOR_ATTACHMENT2)
      glBlitFramebuffer(0, 0, w, h, 0, halfH, halfW, h, GL_COLOR_BUFFER_BIT, GLenum(GL_LINEAR))
    
      r.gbuffer.setReadBuffer(2)
      glBlitFramebuffer(0, 0, w, h, halfW, halfH, w, h, GL_COLOR_BUFFER_BIT, GLenum(GL_LINEAR))
 
      #m_gbuffer.SetReadBuffer(GBuffer::GBUFFER_TEXTURE_TYPE_DIFFUSE);
      #glBlitFramebuffer(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, 0, HalfHeight, HalfWidth, WINDOW_HEIGHT, GL_COLOR_BUFFER_BIT, GL_LINEAR);
 
      #m_gbuffer.SetReadBuffer(GBuffer::GBUFFER_TEXTURE_TYPE_NORMAL);
      #glBlitFramebuffer(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, HalfWidth, HalfHeight, WINDOW_WIDTH, WINDOW_HEIGHT, GL_COLOR_BUFFER_BIT, GL_LINEAR);
 
      #m_gbuffer.SetReadBuffer(GBuffer::GBUFFER_TEXTURE_TYPE_TEXCOORD);
      #glBlitFramebuffer(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, HalfWidth, 0, WINDOW_WIDTH, HalfHeight, GL_COLOR_BUFFER_BIT, GL_LINEAR);
    
    ## Light pass ##

    #if true:
    if not r.showGbuffer:

      r.geometryShader.unbind()
      r.lightingShader.bindd()

      glActiveTexture(GL_TEXTURE0)
      glBindTexture(GL_TEXTURE2D, r.gbuffer.colorTex.tex)
      r.lightingShader.passUniform("tDiffuse", 0, check = true)
  
      glActiveTexture(GL_TEXTURE1)
      glBindTexture(GL_TEXTURE2D, r.gbuffer.positionTex.tex)
      r.lightingShader.passUniform("tPosition", 1, check = true)
    
      glActiveTexture(GL_TEXTURE2)
      glBindTexture(GL_TEXTURE2D, r.gbuffer.normalTex.tex)
      r.lightingShader.passUniform("tNormals", 2, check = true)
    
      r.lightingShader.passUniform("cameraPosition", r.cam.transform.pos, check = true)
      #r.lightingShader.passUniform("lightPosition", r.scene.light0.transform.pos, check = true)
      let lightNodes = r.scene.getNodesWithComponent(Light)
      for i, n in lightNodes:
        let lc = n.get(Light)
        r.lightingShader.passUniform("lights[$1].position".format(i), n.transform.pos, check = true)
        r.lightingShader.passUniform("lights[$1].color".format(i), lc.color, check = true)
    
      r.renderFsQuad()
    
      r.lightingShader.unbind()

      # Cleanup
      glActiveTexture(GL_TEXTURE2)
      glBindTexture(GL_TEXTURE_2D, 0)
      glActiveTexture(GL_TEXTURE1)
      glBindTexture(GL_TEXTURE_2D, 0)
      glActiveTexture(GL_TEXTURE0)
      glBindTexture(GL_TEXTURE_2D, 0)

      ##                     
      ## Shading-less rendering
      ##
            
      r.renderRenderables()
    
  else:
    quit "unknown render type"

  # Unbind all fbos
  glBindFramebufferEXT(GL_FRAMEBUFFER, 0)
  r.finalize()
