import opengl
import ry_types
var gTextureSys*: TextureSys
import framebuffer, glex
import vars as vars


proc initTextureSys*() =
    #TextureSys()
    let s = TextureSys()
    #s.readFbo = newFramebuffer(vars.resX, vars.resY, false)
    s.readFbo = newFramebuffer(256, 256, false)
    s.writeFbo = newFramebuffer(vars.resX, vars.resY, false)
    gTextureSys = s


proc copyTexture*(s: TextureSys, src, dst: Texture) =
    #discard #[
    #discard
    #Blit FBO to normal frame buffer
    #
    #[
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0)
    glDrawBuffer(GL_BACK)

    glBindFramebuffer(GL_READ_FRAMEBUFFER, s.readFbo.fbo)
    glReadBuffer(GL_COLOR_ATTACHMENT0)
    ]#

    #glBlitFramebuffer(GLint(0), GLint(0), GLint(vars.resX), GLint(vars.resY), GLint(0), GLint(0),
    #    GLint(vars.resY), GLint(vars.resY), GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT, GL_NONE)
    glBlitFramebuffer(GLint(0), GLint(0), GLint(256), GLint(256), GLint(0), GLint(0),
        GLint(256), GLint(256), GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT, GLenum(GL_NEAREST))
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0)
    #discard glcheck()
    #]#


proc copyTex*(s: TextureSys, src, dst: Gluint) =
  quit "not implemented"
