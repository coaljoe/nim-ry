import opengl
import ry_types
import framebuffer, glex

proc newRenderTarget*(name: string, width, height: int, depth: bool):
  RenderTarget =

  let rt = RenderTarget(
    name: name,
    width: width,
    height: height,
    depth: depth,
    fb: newFramebuffer(width, height, depth))
  #rt.create()
  glcheck()

  # register element
  #_RenderTargetSys.Add(rt)
  rt

proc tex*(rt: RenderTarget): GLuint = rt.fb.tex

proc bindd*(rt: RenderTarget) =
  rt.fb.bindd()

proc unbind*(rt: RenderTarget) =
  rt.fb.unbind()

