import tables, glm
import scene, node, nodeex, camera, light, transform, vars
export tables

proc setDefaultScene*(s: Scene) =
  #s.clear()
  # Camera
  let aspect = float(vars.resX) / float(vars.resY)
  assert aspect != NaN
  let camNode = newCameraNode()
  camNode.name = "Default Camera"
  s.addNode(camNode)
  #echo aspect
  #quit 2
  camNode.get(Camera).setupView(45, aspect, 0.1, 1000, true)
  s.camera = camNode
  
  # Move camera up
  camNode.get(Transform).pos = vec3f(0, 0, 10)

  # Light
  let l0 = newLightNode()
  l0.name = "Default Light"
  s.addNode(l0)
  l0.get(Transform).pos = vec3f(5, 5, 5)
  #l0.get(Light).intensity = 1.5
  #l0.get(Light).intensity = 2.0
  s.light0 = l0
