## TODO: rename to glutil?
import system, os, strutils, opengl

proc glcheck* =
  let err = glGetError()
  if err != GL_NO_ERROR:
    quit "glcheck fail"


proc compileShader (source: string, shaderType: GLenum) : uint32 =
  #makeContextCurrent()

  let shader = glCreateShader(shaderType)

  #csource := gl.Str(source)
  #gl.ShaderSource(shader, 1, &csource, nil)

  #let csources, free = glStrs(source)
  var s = source.cstring
  glShaderSource(shader, 1, cast[cstringArray](s.addr), nil)
  #glShaderSource(shader, 1, csources, nil)
  #free()
  glCompileShader(shader)

  var status: int32
  glGetShaderiv(shader, GL_COMPILE_STATUS, addr status)
  if status == 0:
    var logLength: int32
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, addr logLength)

    var log = repeat("\x00", int(logLength+1))
    glGetShaderInfoLog(shader, logLength, nil, log.cstring)

    # save shader source
    var ext: string
    case shaderType:
    of GL_VERTEX_SHADER:
      ext = "vert"
    of GL_FRAGMENT_SHADER:
      ext = "frag"
    else:
      quit("unknown shaderType")
    
    let fout = os.joinPath(os.getTempDir(), "crash."&ext)

    var f: File
    try:
      f = open(fout, fmWrite)
    except:
      echo("can't save shader to " & fout)
      quit -1
    f.write(source)
    f.close()
    echo("shader saved to " & fout)

    case ext:
    of "vert":
      echo("vs compile error")
    of "frag":
      echo("fs compile error")
    else:
      quit "unknown ext: " & ext
    
    #panic(fmt.Errorf("failed to compile (log:\n%v)", log))
    quit "failed to compile (log:\n$1)".format(log)

  shader


proc newProgram* (vertexShaderSource, fragmentShaderSource: string) : (GLuint, bool) =
  ## Returns (program, err).
  #makeContextCurrent()

  let
    vertexShader = compileShader(vertexShaderSource, GL_VERTEX_SHADER)
    fragmentShader = compileShader(fragmentShaderSource, GL_FRAGMENT_SHADER)

  let
    program = glCreateProgram()

  echo "program: ", program
  glAttachShader(program, vertexShader)
  glAttachShader(program, fragmentShader)
  glLinkProgram(program)

  var status: int32
  glGetProgramiv(program, GL_LINK_STATUS, addr status)
  if status == 0:
    var logLength: int32
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, addr logLength)

    var log = repeat("\x00", int(logLength+1))
    glGetProgramInfoLog(program, logLength, nil, log)

    #return 0, fmt.Errorf(fmt.Sprintf("failed to link program: %v", log))
    quit "failed to link program: $1".format(log)
  
  glDeleteShader(vertexShader)
  glDeleteShader(fragmentShader)
  echo "xprogram: ", program
  (program, false)

proc createTex*(width, height: int, depth: bool): GLuint =
  var tex: GLuint
  glGenTextures(1, addr tex)
  #tex.Bind(GL_TEXTURE_2D)
  glBindTexture(GL_TEXTURE_2D, tex)

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
  #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
  #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)

  if not depth:
    glTexImage2D(GL_TEXTURE_2D, 0, GLint(GL_RGBA8),
      GLsizei(width), GLsizei(height), 0, GL_RGBA, cGL_UNSIGNED_BYTE, nil)
  else:
    glTexImage2D(GL_TEXTURE_2D, 0, GLint(GL_DEPTH_COMPONENT24),
      GLsizei(width), GLsizei(height), 0, GL_DEPTH_COMPONENT, cGL_FLOAT, nil)

    #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL)
    #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE)
  
  #tex.Unbind(GL_TEXTURE_2D)
  glBindTexture(GL_TEXTURE_2D, 0)

  return tex
