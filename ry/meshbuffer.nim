import opengl, glex

type
  MeshBuffer* = ref object
    fromRawData: bool
    positions: seq[float32] # XXX: use float32?
    normals: seq[float32]
    texcoords: seq[float32]
    indices*: seq[uint32]
    numPositions*: int
    positionsRaw*: string
    normalsRaw*: string
    texcoordsRaw*: string
    hasPositions*: bool
    hasNormals*: bool
    hasTexCoords*: bool
    hasIndices*: bool
    ## gl
    vBuf: GLuint
    nBuf: GLuint
    tBuf: GLuint
    iBuf: GLuint


proc newMeshBuffer*(): MeshBuffer =
  let mb = MeshBuffer(
    fromRawData: false,
    #positions: @[],
    #normals: @[],
    #texcoords: @[],
    #indices: @[])
    )
  mb


proc getPositions*(mb: MeshBuffer): seq[float32] =
  if mb.fromRawData:
    echo "warning: the meshbuffer is created from raw data, " &
         "thus has no positions values"
  mb.positions


proc getNormals*(mb: MeshBuffer): seq[float32] =
  if mb.fromRawData:
    echo "warning: the meshbuffer is created from raw data, " &
         "thus has no normals values"
  mb.normals


proc getTexcoords*(mb: MeshBuffer): seq[float32] =
  if mb.fromRawData:
    echo "warning: the meshbuffer is created from raw data, " &
         "thus has no texcoords values"
  mb.texcoords


proc prepare(mb: MeshBuffer) =
  if not mb.fromRawData:
    mb.positions = @[]
    mb.normals = @[]
    mb.texcoords = @[]
    mb.indices = @[]
  else:
    mb.positionsRaw = ""
    mb.normalsRaw = ""
    mb.texcoordsRaw = ""


proc build(mb: MeshBuffer) =
  # check before for previous errrors
  glcheck()

  let sizeGLfloat: int = 4
  let sizeGLuint: int = 4
  #let sizeGLfloat = sizeof(GLfloat)

  #[
  if len(mb.positions) > 0:
    mb.hasPositions = true
    glGenBuffers(1.GLsizei, addr mb.vBuf)
    glcheck()
    glBindBuffer(GL_ARRAY_BUFFER, mb.vBuf)
    glcheck()
    glBufferData(GL_ARRAY_BUFFER, GLsizeiptr(len(mb.positions)*sizeGLfloat), addr mb.positions[0], GL_STATIC_DRAW)
    glcheck()
    quit -1
  ]#

  if len(mb.positions) > 0 or len(mb.positionsRaw) > 0:
    mb.hasPositions = true
    glGenBuffers(1.GLsizei, addr mb.vBuf)
    glcheck()
    glBindBuffer(GL_ARRAY_BUFFER, mb.vBuf)
    glcheck()
    if not mb.fromRawData:
      mb.numPositions = len(mb.positions)
      glBufferData(GL_ARRAY_BUFFER, GLsizeiptr(len(mb.positions)*sizeGLfloat), addr mb.positions[0], GL_STATIC_DRAW)
    else:
      mb.numPositions = len(mb.positionsRaw) div sizeGLfloat
      glBufferData(GL_ARRAY_BUFFER, GLsizeiptr(mb.numPositions*sizeGLfloat), addr mb.positionsRaw[0], GL_STATIC_DRAW)
    glcheck()
    #quit -1

  #[
  if len(mb.positionsRaw) > 0:
      mb.hasPositions = true
      mb.numPositions = len(mb.positionsRaw) div sizeGLfloat
      glGenBuffers(1.GLsizei, addr mb.vBuf)
      glcheck()
      glBindBuffer(GL_ARRAY_BUFFER, mb.vBuf)
      glcheck()
      #glBufferData(GL_ARRAY_BUFFER, GLsizeiptr(len(mb.positions)*sizeGLfloat), addr mb.positionsRaw[0], GL_STATIC_DRAW)
      #glBufferData(GL_ARRAY_BUFFER, GLsizeiptr(len(mb.positions) * sizeGLfloat), addr mb.positionsRaw[0], GL_STATIC_DRAW)
      glBufferData(GL_ARRAY_BUFFER, GLsizeiptr(mb.numPositions*sizeGLfloat), addr mb.positionsRaw[0], GL_STATIC_DRAW)
      glcheck()
      echo len(mb.positionsRaw)
      echo ">", mb.positionsRaw, "<"
      #quit 2
  ]#
  
  #[
  if len(mb.normals) > 0:
    mb.hasNormals = true
    glGenBuffers(1, addr mb.nBuf)
    glBindBuffer(GL_ARRAY_BUFFER, mb.nBuf)
    glBufferData(GL_ARRAY_BUFFER, GLsizeiptr(len(mb.normals)*sizeGLfloat), addr mb.normals[0], GL_STATIC_DRAW)
  if len(mb.texcoords) > 0:
    mb.hasTexCoords = true
    glGenBuffers(1, addr mb.tBuf)
    glBindBuffer(GL_ARRAY_BUFFER, mb.tBuf)
    glBufferData(GL_ARRAY_BUFFER, GLsizeiptr(len(mb.texcoords)*sizeGLfloat), addr mb.texcoords[0], GL_STATIC_DRAW)
  if len(mb.indices) > 0:
    mb.hasIndices = true
    glGenBuffers(1, addr mb.iBuf)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mb.iBuf)
    #glBufferData(GL_ELEMENT_ARRAY_BUFFER, GLsizeiptr(len(mb.indices)), addr mb.indices[0], GL_STATIC_DRAW)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, GLsizeiptr(len(mb.indices)*sizeGLuint), addr mb.indices[0], GL_STATIC_DRAW)
  ]#

  if len(mb.normals) > 0 or len(mb.normalsRaw) > 0:
    mb.hasNormals = true
    glGenBuffers(1, addr mb.nBuf)
    glBindBuffer(GL_ARRAY_BUFFER, mb.nBuf)
    if not mb.fromRawData:
      glBufferData(GL_ARRAY_BUFFER, GLsizeiptr(len(mb.normals)*sizeGLfloat), addr mb.normals[0], GL_STATIC_DRAW)
    else:
      glBufferData(GL_ARRAY_BUFFER, GLsizeiptr((len(mb.normalsRaw) div sizeGLfloat) * sizeGLfloat), addr mb.normalsRaw[0], GL_STATIC_DRAW)
    glcheck()
    
  if len(mb.texcoords) > 0 or len(mb.texcoordsRaw) > 0:
    mb.hasTexCoords = true
    glGenBuffers(1, addr mb.tBuf)
    glBindBuffer(GL_ARRAY_BUFFER, mb.tBuf)
    if not mb.fromRawData:
      glBufferData(GL_ARRAY_BUFFER, GLsizeiptr(len(mb.texcoords)*sizeGLfloat), addr mb.texcoords[0], GL_STATIC_DRAW)
    else:
      glBufferData(GL_ARRAY_BUFFER, GLsizeiptr((len(mb.texcoordsRaw) div sizeGLfloat) * sizeGLfloat), addr mb.texcoordsRaw[0], GL_STATIC_DRAW)
    glcheck()
    
  if len(mb.indices) > 0:
    mb.hasIndices = true
    glGenBuffers(1, addr mb.iBuf)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mb.iBuf)
    #glBufferData(GL_ELEMENT_ARRAY_BUFFER, GLsizeiptr(len(mb.indices)), addr mb.indices[0], GL_STATIC_DRAW)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, GLsizeiptr(len(mb.indices)*sizeGLuint), addr mb.indices[0], GL_STATIC_DRAW)
    glcheck()

  glcheck()

  # unbind buffers
  glBindBuffer(GL_ARRAY_BUFFER, 0)
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)


proc render*(mb: MeshBuffer) =
  if not mb.hasPositions:
    quit("no positions")
  if not mb.hasNormals:
    quit("no normals")

  #echo mb.nBuf, " ", mb.vBuf
  #echo len(mb.normalsRaw), " ", len(mb.positionsRaw)
  
  glEnableClientState(GL_VERTEX_ARRAY)
  glEnableClientState(GL_NORMAL_ARRAY)
  #[	
		if mb.hasNormals {
			gl.EnableClientState(gl.NORMAL_ARRAY)
		}
  ]#
  if mb.hasTexCoords:
    glEnableClientState(GL_TEXTURE_COORD_ARRAY)

  # Vertex
  glBindBuffer(GL_ARRAY_BUFFER, mb.vBuf)
  glVertexPointer(3, cGL_FLOAT, 0.GLsizei, nil)
  #glVertexPointer(3, cGL_FLOAT, GLsizei(sizeof(GLfloat)), addr mb.vBuf)
  #glVertexPointer(3, cGL_FLOAT, 0, mb.positions.addr)

  # Normals
  glBindBuffer(GL_ARRAY_BUFFER, mb.nBuf)
  glNormalPointer(cGL_FLOAT, 0.GLsizei, nil)

  #[
    if mb.hasNormals {
      gl.BindBuffer(gl.ARRAY_BUFFER, mb.nBuf)
      gl.NormalPointer(gl.FLOAT, 0, nil)
    }
  ]#
  if mb.hasTexCoords:
    glClientActiveTexture(GL_TEXTURE0)
    glBindBuffer(GL_ARRAY_BUFFER, mb.tBuf)
    glTexCoordPointer(2, cGL_FLOAT, 0.GLsizei, nil) # UV
    #glTexCoordPointer(3, cGL_FLOAT, 0.GLsizei, nil) # UVW

  if mb.hasIndices:
    #quit("unsupported rendering mode")
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mb.iBuf)
    #echo mb.indices
    #echo int32(len(mb.indices)*3)
    #quit 2
    glDrawElements(GL_TRIANGLES, GLint(len(mb.indices)), GL_UNSIGNED_INT, nil)
    #glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, nil)
    # can be UNSIGNED_SHORT
  else:
    glDrawArrays(GL_TRIANGLES, int32(0), int32(mb.numPositions/3))
    #glDrawArrays(GL_TRIANGLES, int32(0), int32(len(mb.positions)/3))
    #gl.DrawArrays(gl.TRIANGLES, int32(0), 1)
    #gl.DrawArrays(gl.TRIANGLE_STRIP, int32(0), int32(len(mb.positions)/4))
    glcheck()
  

  glDisableClientState(GL_VERTEX_ARRAY)
  glDisableClientState(GL_NORMAL_ARRAY)
  #[
    if mb.hasNormals {
      gl.DisableClientState(gl.NORMAL_ARRAY)
    }
  ]#
  if mb.hasTexCoords:
    glDisableClientState(GL_TEXTURE_COORD_ARRAY)

  glcheck()

  glBindBuffer(GL_ARRAY_BUFFER, 0)
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)


proc add*(mb: MeshBuffer, positions, normals, texcoords: seq[GLfloat],
           indices: seq[uint32]) =
  ## Create MeshBuffer from non-raw data
  mb.fromRawData = false
  mb.prepare()
  mb.positions = positions
  mb.normals = normals
  mb.texcoords = texcoords
  mb.indices = indices
  mb.build()


proc addRawData*(mb: MeshBuffer, positions, normals, texcoords: string) =
  ## Create MeshBuffer from raw data
  mb.fromRawData = true
  mb.prepare()
  mb.positionsRaw = positions
  mb.normalsRaw = normals
  mb.texcoordsRaw = texcoords
  mb.build()

