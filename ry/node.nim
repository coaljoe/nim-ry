import strutils, ecs
import transform
export ecs, transform#, light

#var maxNodeId = -1

type
  NodeType* {.pure.} = enum
    Unknown
    Empty
    Mesh
    Light
    Camera
    Custom

  Node* = ref object of Entity
    ## Base of all nodes in the scene.
    ## XXX: rename to NodeData component?
    #nodeId*: int
    name*: string
    nodetype*: NodeType
    visible*: bool
    parent: Node
    children: seq[Node]
    


#proc id*(n: Node): int = n.Entity.id()
proc id*(n: Node): int = n.Entity.id()
  

proc newNode*(typ: NodeType): Node =
  #maxNodeId += 1
  let n = Node(nodetype: typ, visible: true)
  n.initEntity()
  echo "[Node] newNode id: ", n.id(), " type: ", n.nodetype
  # XXX add nodes to world?
  n


proc newEmptyNode*(): Node =
  let n = newNode(NodeType.Empty)
  n.addComponent(newTransform())
  n


proc transform*(n: Node): Transform =
  ## Transform component shortcut
  n.get(Transform)


proc tc*(n: Node): Transform =
  ## Shortcut to for Transform component
  n.transform()


proc `$`*(n: Node): string =
  var s = "Node<id=$1 type=$2".format(n.id(), n.nodetype)
  if n.name != nil:
    s &= " name='$1'".format(n.name)
  s & ">"
