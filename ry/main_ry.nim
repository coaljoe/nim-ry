when defined(windows):
  # compile with: nim c -d:glfwDynlib main_ry.nim
  var glfwDynlib: string

#import lib/ry/ry
#import lib/ry/node
#import lib/ry/ecs
#import ../../ecs
import random, collections/iface, opengl, glm, fp/option, ecs
import ry_types
import ry, node, mesh, light, material, shader, scene, sceneloader, renderer, renderpass,
  card, spritemesh, nodeex, trenderer, vars

proc main() =
  echo "main()"
  let ry = newRy()
  ry.init(960, 540)
  ry.renderer.setRenderType(RenderType.Deferred)

  ## Empty node test
  var qn = newEmptyNode()
  echo qn
  let trans = qn.get(Transform)
  echo trans.pos
  #echo trans.mat
  #quit(2)
  trans.pos = vec3f(2, 3, 4)
  echo trans.pos

  ## Light node test
  var ln = newLightNode()
  let li = ln.get(Light)
  echo li.color
  li.color = Vec3Zero
  li.color.x = 1.0f
  echo li.color
  #assert l.color == Vec3f(1f, 0f, 0f)
  
  let nodes = @[qn, ln]
  echo nodes

  echo trans.mat
  trans.rot = vec3f(45, 45, 45)
  echo trans.mat

  #discard """
  let sl = newSceneLoader()
  #let xnodes = sl.load("/tmp/cube.dae")
  #let xnodes = sl.load("tmp/teapot.obj")
  #let xnodes = sl.load("tmp/cube.obj")
  let xnodes = sl.load("primitives/box_textured/box_textured.obj")
  #let xnodes = sl.load("primitives/square_textured/square_textured.obj")
  #let xnodes = @[ry.resourcesys.loadMesh("../tmp/dragon.obj")]
  #let xnodes = sl.load("tmp/triangle1.obj")
  let mn = xnodes[0]
  echo xnodes
  echo mn.get(Mesh).mb.indices
  echo mn.get(Mesh).mb.hasIndices
  echo mn.get(Mesh).mb.hasTexcoords
  echo "len pos: ", len(mn.get(Mesh).mb.getPositions())
  echo mn.get(Mesh).mb.getPositions()
  echo "len texcoords: ", len(mn.get(Mesh).mb.getPositions())
  echo mn.get(Mesh).mb.getTexcoords()
  #echo sizeof(GLfloat), " ", sizeof(float32), " ", sizeof(float)
  #quit 2
  #echo xnodes[0].get("Mesh").Mesh.mb.indices
  #echo ry.scene.nodes
  #ry.scene.addNodes(xnodes)
  ry.scene.addNode(mn)
  echo ry.scene.getNodes
  #mn.get(Transform).pos = vec3f(1, 1, 0)
  #mn.get(Transform).pos = vec3f(-20, -5, 0)
  #ry.scene.getNodes[0].get(Transform).pos = vec3f(1, 1, 0)
  #let mn = ry.scene.getNodes[0]
  #let mn = ry.scene.getNodeById(xnodes[0].id)

  ## setup camera
  #ry.scene.camera.get(Transform).pos = vec3f(0, 0, -10)
  ry.scene.camera.get(Transform).pos = vec3f(0, 0, 10)

  # Default material
  let defaultMat = newMaterial("materials/model.json")
  defaultMat.name = "defaultMat"
  defaultMat.diffuse = vec3f(0.2, 0.5, 0.2)
  defaultMat.specular = vec3f(0.9)
  defaultMat.ambient = vec3f(0.2)
  #defaultMat.hardness = 10
  defaultMat.hardness = 1
  #defaultMat.setTexture("primitives/box_textured/texture.png")
  #defaultMat.setTexture("primitives/square_textured/texture.png")
  #defaultMat.setTexture("primitives/box_textured/texture.jpg")
  defaultMat.setTexture("textures/checker.png")
  #mn.get(Mesh).setMaterial(defaultMat)
  
  if isNil(defaultMat.shader):
    quit "material is nil"
  #matDefault.load()
  
  # mat2
  let mat2 = newMaterial("materials/model.json")
  mat2.name = "defaultMat"
  mat2.diffuse = vec3f(1.0, 1.0, 1.0)
  mat2.specular = vec3f(0.9)
  mat2.ambient = vec3f(0.2)
  mat2.hardness = 16
  mat2.setTexture("textures/solid_white.png")
  
  let m = mn.get(Mesh)
  #m.setMaterial(defaultMat)
  m.setMaterial(mat2)

  if false:
    let rp1 = newRenderPass("testRP")
    ry.renderer.addRenderPass(rp1)
    ry.renderer.renderPasses[0].render(ry.renderer)
  #quit 2

  #let c1 = newCard()
  #c1.setDim(0, 0, 0.5, 0.5)
  #ry.renderer.renderables.add(c1)
  #ry.renderer.renderables.add(asRenderableI(c1))

  #var mn2: Node
  let mn2 = ry.resourcesys.loadMesh("primitives/box_textured/box_textured.obj")
  #mn2.get(Mesh).setMaterial(defaultMat)
  mn2.get(Mesh).setMaterial(mat2)
  gScene.addNode(mn2)
  
  var rotNodes: seq[Node]
  rotNodes = @[]
  
  var gridNodes: seq[Node]
  gridNodes = @[]
  var rotGridNode: Node

  block:
    if false:
      let smn = newSpriteMeshNode()
      let sm = smn.get(SpriteMesh)
      #let sm = newSpriteMesh()
      #sm.load("primitives/square_textured/square_textured.obj")
      sm.load("tmp/comtess armand.obj")
      #sm.load("primitives/square_textured/square_textured.obj")
      #sm.load("tmp/dragon.obj")
      #sm.spawn()
      #ry.scene.addNode(sm.node)
      echo sm.node
      #mn2 = sm.node
      #sm.node.get(Transform).pos = vec3f(0, 0, 0)
      let tc = sm.node.get(Transform)
      tc.pos = vec3f(5, 5, 0)
      #tc.rot = vec3f(45, 0, 0)
      tc.rot = vec3f(-90, -90, 0)
      #sm.node.get(Transform).pos = vec3f(1, 1, 0)
      #quit 2
      # FIXME: order is important
      ry.renderer.renderables.add(asRenderableI(sm.card))
      ry.renderer.renderables.add(asRenderableI(sm))
      rotNodes.add(sm.node)
    
  block:
    if false:
      let sm = newSpriteMesh()
      #sm.load("primitives/square_textured/square_textured.obj")
      #sm.load("tmp/comtess armand.obj")
      #sm.load("primitives/square_textured/square_textured.obj")
      sm.load("tmp/dragon.obj")
      #sm.spawn()
      #ry.scene.addNode(sm.node)
      echo sm.node
      #mn2 = sm.node
      #quit 2
      #mn2 = sm.node
      #sm.node.get(Transform).pos = vec3f(0, 0, 0)
      let tc = sm.node.get(Transform)
      tc.pos = vec3f(5, 5, 0)
      #tc.rot = vec3f(45, 0, 0)
      tc.rot = vec3f(-90, -90, 0)
      #sm.node.get(Transform).pos = vec3f(1, 1, 0)
      #quit 2
      # FIXME: order is important
      ry.renderer.renderables.add(asRenderableI(sm.card))
      ry.renderer.renderables.add(asRenderableI(sm))
      rotNodes.add(sm.node)
    
      #
      #sm.card.setDim(0.25, 0.25, 0.5, 0.5)
      sm.card.setDim(0.5, 0.0, 0.5, 0.5)

  block:
    if false:
      let n = 8  # Side
      #let spacingX = 3.0
      #let spacingY = 4.0
      let spacingX = 5.0
      let spacingY = 5.0
      let offsetX = -20.0
      let offsetY = -10.0
      for x in 0..<n:
        for y in 0..<n:
          echo "spawning mesh n: ", x*n+y, " xy: ", x, ",", y
          #let mn = ry.resourcesys.loadMesh("tmp/dragon.obj")
          #let mn = ry.resourcesys.loadMesh("tmp/appartment building 12 storey.obj")
          let mn = ry.resourcesys.loadMesh("primitives/box_textured/box_textured.obj")
          ry.scene.addNode(mn)
          let trans = mn.get(Transform)
          trans.pos = vec3f(offsetX + spacingX*x.toFloat, offsetY + spacingY*y.toFloat, 0)
          #echo trans.pos
          gridNodes.add(mn)
      echo ry.scene.nodes
      #quit 2
      rotGridNode = gridNodes[0]
    
  block:
    if true:
      #let n = 80  # Side
      #let n = 20  # Side
      let n = 8  # Side
      #let spacingX = 3.0
      #let spacingY = 4.0
      let spacingX = 0.8
      let spacingY = 0.8
      let offsetX = 0.0 #-10.0
      let offsetY = 0.0 #-5.0
      for x in 0..<n:
        for y in 0..<n:
          echo "spawning mesh n: ", x*n+y, " xy: ", x, ",", y
          #let mn = ry.resourcesys.loadMesh("../tmp/dragon.obj")
          #let mn = ry.resourcesys.loadMesh("tmp/appartment building 12 storey.obj")
          #let mn = ry.resourcesys.loadMesh("primitives/box_textured/box_textured.obj")
          let mn = ry.resourcesys.loadMesh("models/tree/tree.obj")
          let trans = mn.get(Transform)
          let posAdj = vec3f(random(spacingX/2), random(spacingY/2), 0.0)
          trans.pos = vec3f(offsetX + spacingX*x.toFloat, offsetY + spacingY*y.toFloat, 0) + posAdj
          trans.scale = trans.scale * 0.25 * max(0.8, random(1.0))
          trans.rot = vec3f(trans.rot.x, trans.rot.y, random(360.0))
          #echo trans.pos
          mn.get(Mesh).setMaterial(defaultMat)
          #mn.get(Mesh).setMaterial(mat2)
          gScene.addNode(mn)
          gridNodes.add(mn)
      echo ry.scene.nodes
      #quit 2
      rotGridNode = gridNodes[0]

  block:
    if false:
      #let n = 16  # Side
      #let n = 8
      let n = 2
      #let spacingX = 3.0
      #let spacingY = 4.0
      let spacingX = 5.0
      let spacingY = 5.0
      let offsetX = -8.0
      let offsetY = -4.0
      for x in 0..<n:
        #for y in 0..<n:
        for y in 0..0:
          echo "spawning spritemesh n: ", x*n+y, " xy: ", x, ",", y
          let sm = newSpriteMesh()
          sm.load("../tmp/dragon.obj")
          #sm.load("primitives/square_textured/square_textured.obj")
          ry.renderer.renderables.add(asRenderableI(sm.card))
          ry.renderer.renderables.add(asRenderableI(sm))
          let node = sm.node()
          node.get(Mesh).setMaterial(defaultMat)
          # Only adding meshes to the scene for debug rendering
          #ry.scene.addNode(node)
          let trans = node.get(Transform)
          trans.pos = vec3f(offsetX + spacingX*x.toFloat, offsetY + spacingY*y.toFloat, 0)
          #echo trans.pos
          gridNodes.add(node)
      echo ry.scene.nodes
      rotGridNode = gridNodes[0]
      #quit 2
      
  block:
    if false:
      #let n = 16  # Side
      #let n = 8
      let n = 32
      #let spacingX = 3.0
      #let spacingY = 4.0
      let spacingX = 0.5
      let spacingY = 0.5
      let offsetX = -8.0
      let offsetY = -4.0
      for x in 0..<n:
        for y in 0..<n:
          echo "spawning spritemesh n: ", x*n+y, " xy: ", x, ",", y
          let sm = newSpriteMesh()
          sm.load("primitives/box_textured/box_textured.obj")
          #echo "mn: mn
          ry.renderer.renderables.add(asRenderableI(sm.card))
          ry.renderer.renderables.add(asRenderableI(sm))
          let node = sm.node
          node.get(Transform).scale = vec3f(0.1, 0.1, 0.1)
          #node.get(Mesh).setMaterial(defaultMat)
          #ry.scene.addNode(node)
          let trans = node.get(Transform)
          trans.pos = vec3f(offsetX + spacingX*x.toFloat, offsetY + spacingY*y.toFloat, 0)
      #quit 2

  #mn.visible = false


  var tr: TRenderer
  block:
    tr = newTRenderer(5, 5)
    #tr = newTRenderer(3, 3)
    #tr = newTRenderer(2, 2)
    #tr = newTRenderer(4, 1)
    #tr = newTRenderer(2, 1)
    #tr = newTRenderer(1, 1)
    ry.renderer.renderables.add(asRenderableI(tr))

  if true:
    # Tilt camera
    let tc = gScene.camera.transform
    tc.pos = vec3f(0, 0, 0)
    #tc.rot = vec3f(50, 0, 45)
    tc.rot = vec3f(45, 0, 45)
    #tc.forceBuild()
    tc.translateLocal(vec3f(0, 0, 20))
    tc.forceBuild()
    #tc.translateLocal(vec3f(0, 0, -10))
    # Flipped-z coords
    #tc.rot = Vec3(-50, 180, 180+-45)
    #tc.translateLocal(Vec3(0, 0, 10))
    #ecs.getEntity(Scene.camera).get<Transform>().rot = Vec3(60, 0, 45)
    #ecs.getEntity(Scene.camera).get<Transform>().rot = Vec3(75, 0, 45)
    #ecs.getEntity(Scene.camera).get<Transform>().rot = Vec3(0, 0, -45)
    #ecs.getEntity(Scene.camera).get<Transform>().rot = Vec3(0, 0, 90)

    # Update tr
    tr.tcamInitialPos = some tc.pos
    echo tc.pos
    echo tc
    #quit 2

  #quit 1
  
  # Move meshes to TRenderer
  if true:
    for n in gScene.getNodes:
      if n.nodetype == NodeType.Mesh:
        #if n != mn:
        #  continue
        if n == mn2:
          continue
        tr.addNode(n)
    tr.refresh()
   
  # XXX remove
  #tr.hide()
  tr.showNodes()
  #quit 2

  let rotSpeed = 20.0
  let rotate = true #false
  var l0Angle = 0.0
  let l0RotSpeed = 50.0
  #let l0RotRadius = 120.0
  let l0RotRadius = 10.0
  while ry.step():
    let dt = ry.dt
    #echo "update ", dt
    if rotate:
      let mnT = mn.get(Transform)
      let oldRot = mnT.rot
      #mnT.rot = oldRot + vec3f(0.0, 0.0, rotSpeed * dt)
      mnT.rot = oldRot + vec3f(rotSpeed * dt, rotSpeed * dt, rotSpeed * dt)
      #echo mnT.rot, mnT.pos

      
    ## Rotate grid nodes
    if false and len(gridNodes) > 0:
      #echo gridNodes
      #quit 2
      if ry.app.frame mod 1000 == 0:
        rotGridNode = random(gridNodes)
      var extraRotNodes = rotNodes
      if rotGridNode != nil:
        extraRotNodes.add(rotGridNode)
      #extraRotNodes.add(random(gridNodes))
      for n in extraRotNodes:
        let mn = n
        let mnT = mn.get(Transform)
        let oldRot = mnT.rot
        mnT.rot = oldRot + vec3f(rotSpeed * dt, rotSpeed * dt, rotSpeed * dt)
      #discard
      
    ## Remove random meshes from TRenderer
    if false and ry.app.frame mod 100 == 0:
      let randomNode = random(tr.nodes)
      tr.removeNode(randomNode)
      tr.refresh()
      
    ## Remove random scene meshes
    if false and ry.app.frame mod 10 == 0:
      let randomNode = random(gScene.getNodes)
      gScene.removeNode(randomNode)
      
    if true:
      let a = radians(l0Angle)
      let l0 = gScene.light0
      let rm = mat4f(1.0).rotate(vec3f(0, 0, 1), a)
      #l0.transform.pos = vec3f(sin(a), cos(a), l0.transform.pos.z)
      #mn.transform.pos = vec3f(l0RotRadius * sin(a), l0RotRadius * cos(a), mn.transform.pos.z)
      #mn.transform.pos = vec3f(l0RotRadius * cos(a), l0RotRadius * sin(a), mn.transform.pos.z)
      #mn.transform.pos = vec3f(l0RotRadius * cos(a), l0RotRadius * sin(a), 0)
      l0.transform.pos = vec3f(l0RotRadius * cos(a), l0RotRadius * sin(a), 5)
      #l0.transform.pos = vec3f(l0RotRadius * cos(a), l0RotRadius * sin(a), 15)
      #mn.transform.pos = vec3f(l0RotRadius * cos(a), mn.transform.pos.y, l0RotRadius * sin(a))
      #let npos = (rm * vec4f(mn.transform.pos, 1.0))
      #mn.transform.pos = vec3f(npos.x, npos.y, npos.z)
      #mn.transform.pos = vec3f(5, 5, 0)
      #mn.transform.rot = vec3f(npos.x, npos.y, npos.z)
      l0Angle += l0RotSpeed * dt
      mn2.transform.pos = l0.transform.pos

  echo "done"


if isMainModule:
  main()
