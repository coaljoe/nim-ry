## An abstraction over the opengl framebuffer object (FBO).
import opengl, strutils
import glex

type
  Framebuffer* = ref object
    width*: int
    height*: int
    modeDepth*: bool  # Is a depth frame buffer
    onlyColor*: bool  # Always false
    tex*: GLuint
    #gl
    fbo*: GLuint
    rbo_depth*: GLuint  # Depth render buffer object

# Forward decl.
proc create(f: Framebuffer)

proc newFramebuffer*(width, height: int, modeDepth: bool): Framebuffer =
  let f = Framebuffer(
    tex: 0,
    width: width,
    height: height,
    modeDepth: modeDepth,
    onlyColor: false)
  f.create()
  f

proc create(f: Framebuffer) =
  echo "Framebuffer: create $1 $2 $3".format(f.width, f.height, f.modeDepth)
  #f.tex = CreateTexture(f.width, f.height, f.modeDepth)
  f.tex = createTex(f.width, f.height, f.modeDepth)
  glcheck()

  # tune the texture
  if f.modeDepth:
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
  else:
    # need for fxaa
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

  # create FBO
  glGenFramebuffers(1, addr f.fbo)
  #rt.fbo.Bind()
  glBindFramebuffer(GL_FRAMEBUFFER, f.fbo)
  glcheck()

  if f.onlyColor:
    #discard
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
      GL_TEXTURE_2D, f.tex, 0)
    glcheck()
  else:
    # depth buffer
    glGenRenderbuffers(1, addr f.rbo_depth)
    #rbo_depth.Bind()
    glBindRenderbuffer(GL_RENDERBUFFER, f.rbo_depth)
    glcheck()
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24,
      int32(f.width), int32(f.height))
    glcheck()
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
      GL_RENDERBUFFER, f.rbo_depth)
    glcheck()

    # attach the texture
    if not f.modeDepth:
      glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
        GL_TEXTURE_2D, f.tex, 0)
    else:
      glDrawBuffer(GL_NONE)
      glReadBuffer(GL_NONE)
      glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
        GL_TEXTURE_2D, f.tex, 0)
  glcheck()

  # check status
  let status = glCheckFramebufferStatus(GL_FRAMEBUFFER)
  if status != GL_FRAMEBUFFER_COMPLETE:
    quit("Bad FBO, status: " & "$1".format(GLint(status)))
  
  glcheck()

  # unbind
  #rt.fbo.Unbind()
  #rbo_depth.Unbind()
  glBindFramebuffer(GL_FRAMEBUFFER, 0)
  glcheck()

proc bindd*(f: Framebuffer) =
  glBindFramebuffer(GL_FRAMEBUFFER, f.fbo)

proc unbind*(f: Framebuffer) =
  glBindFramebuffer(GL_FRAMEBUFFER, 0)
  
proc attachTexture*(f: Framebuffer, tex: GLuint, idx: int = 0) =
  glFramebufferTexture2D(GL_FRAMEBUFFER, GLenum(GLint(GL_COLOR_ATTACHMENT0) + idx),
      GL_TEXTURE_2D, tex, 0)
  glcheck()

proc clearColor*(f: Framebuffer, r, g, b, a: float) =
  ## Clear fbo with color
  f.bindd()
  glClearColor(r, g, b, a)
  glClear(GL_COLOR_BUFFER_BIT)
  f.unbind()
