import os, node, sceneloader, vars

type
  ResourceSys* = ref object

var gResourceSys*: ResourceSys


proc newResourceSys*(): ResourceSys =
  gResourceSys = ResourceSys()
  gResourceSys


#proc load*[T](rs: ResourceSys, path: string): T =
proc loadMesh*(rs: ResourceSys, path: string): Node =
  let xpath = vars.resPath & path
  if not os.existsFile(xpath):
    quit "path does't exist; path: " & xpath
  let sl = newSceneLoader()
  let nodes = sl.load(path)
  nodes[0]
