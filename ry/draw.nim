import opengl

proc drawLine* (x1, y1, z1, x2, y2, z2: float) =
  glBegin(GL_LINES)
  glVertex3f(x1, y1, z1)
  glVertex3f(x2, y2, z2)
  glEnd()

proc drawGrid* (size, cell_size: float) =
  echo "drawGrid: ", size, " ", cell_size
  glLineWidth(1.0)
  let n = size
  let s = cell_size
  glBegin(GL_LINES)
  var i = 0.0
  while i < n:
    if i == 0:
      glColor3f(0.3, 0.6, 0.3)
    else:
      glColor3f(0.25, 0.25, 0.25)
    glVertex3f(i, 0, 0)
    glVertex3f(i, n, 0)
    if i == 0:
      glColor3f(0.6, 0.3, 0.3)
    else:
      glColor3f(0.25, 0.25, 0.25)
    glVertex3f(0, i, 0)
    glVertex3f(n, i, 0)
    i += s
  glEnd()

proc drawRect*(x, y, w, h: float) =
  drawLine(x, y, 0, x+w, y, 0)      # bottom
  drawLine(x+w, y, 0, x+w, y+h, 0)  # right
  drawLine(x, y+h, 0, x+w, y+h, 0)  # top
  drawLine(x, y, 0, x, y+h, 0)      # left

proc drawSetColor*(r, g, b: float) =
  glColor3f(r, g, b)

proc drawPlane*(scale: float) =
  #gl.BindTexture(gl.TEXTURE_2D, texture)

  #gl.Color4f(1, 1, 1, 1)

  glPushMatrix()
  glScalef(scale, scale, scale)

  let dx = 1.0 / 2
  let dy = 1.0 / 2
  #dz_ := float32(1.0 / 2)

  glBegin(GL_QUADS)

  #gl.Normal3f(0, 0, 1)
  #gl.TexCoord2f(0, 0)
  glVertex3f(-1*dx, -1*dy, 0)
  #gl.TexCoord2f(1, 0)
  glVertex3f(1*dx, -1*dy, 0)
  #gl.TexCoord2f(1, 1)
  glVertex3f(1*dx, 1*dy, 0)
  #gl.TexCoord2f(0, 1
  glVertex3f(-1*dx, 1*dy, 0)
  
  glEnd()

  glPopMatrix()

