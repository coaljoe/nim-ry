import math
import glm

# XXX: fixme: fixes the Mat4Identity bug on windows?
echo "XXX", mat4f()

#[
type
  Vec4f* = Vec4[float32]
  Vec3f* = Vec3[float32]
  Mat4f* = Mat4x4[float32]

#let Vec3Zero* = vec3(0f, 0f, 0f)
#let Vec3One* = vec3(1f, 1f, 1f)

proc vec3f* (x, y, z: float32): Vec3f = [x, y, z].Vec3f
proc vec4f* (x, y, z, w: float32): Vec4f = [x, y, z, w].Vec4f

proc toMat4f (mat: Mat4x4[float]): Mat4f =
  for i in 0 ..< 4:
    for j in 0 ..< 4:
      result[i][j] = mat[i][j].float32
      
#converter toMat4x4[float32] (m: Mat4f): Mat4x4[float32] =
#  Mat4x4[float32](m)

]#

#proc radians* [T](deg:T): T =
#  deg/180*PI

let Vec3Zero* = vec3f(0, 0, 0)
let Vec3One* = vec3f(1, 1, 1)
let Vec3Up* = vec3f(0, 0, 1)
let Vec3Forward* = vec3f(0, 1, 0)
#echo "YYY", mat4f()
#let Mat4Identity* = toMat4f(mat4())
let Mat4Identity* = mat4f() # Creates identity matrix.
echo Mat4Identity
if Mat4Identity[3][3] != 1.0:
  quit "Error: bad Mat4Identity"
#echo Mat4Identity
#echo mat4[float32](float32(1.0))
#echo mat4[4,4,float32](1.0)
#quit 2