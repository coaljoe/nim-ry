import glm, ecs
import glmex, transform

type
  # Component.
  Camera* = ref object of Component
    fov*: float
    aspect*: float
    znear*: float
    zfar*: float
    zoom*: float
    ortho*: bool
    orthoPlanes*: array[4, float] # Readonly?

# Forward decl.
#proc build (c:Camera)
#proc setupView* (c:Camera, fov, aspect, znear, zfar :float, ortho:bool)


proc build*(c: Camera) =
  var
    ortho_scale = c.fov
    aspect_ratio = c.aspect
    xaspect = aspect_ratio
    yaspect = 1.0
  xaspect = xaspect * ortho_scale / (aspect_ratio * 2)
  yaspect = yaspect * ortho_scale / (aspect_ratio * 2)
  c.orthoPlanes = [-xaspect * c.zoom, xaspect * c.zoom,
    -yaspect * c.zoom, yaspect * c.zoom]


proc setupView* (c:Camera, fov, aspect, znear, zfar :float, ortho:bool) =
  c.fov = fov
  c.aspect = aspect
  c.znear = znear
  c.zfar = zfar
  c.ortho = ortho

  if not c.ortho:
    quit "only ortho cams supported"

  c.build()


proc newCamera*(): Camera =
  #[
  let c = Camera(
    fov: 45,
    aspect: 1.0,
    znear: 0.1,
    zfar: 1000,
    ortho: true,
    zoom: 1.0)
  ]#
  var c = Camera(zoom: 1.0)
  c.setupView(45, 1.0, 0.1, 1000, true)
  c


proc getViewMatrix*(c: Camera): Mat4f =
  let tc = c.entity.get(Transform)
  #var m = Mat4x4(tc.mat)
  #var m = Mat4x4[float32](tc.mat).inverse
  #var m = Mat4x4(tc.mat)

  # XXX fixme: add non-inversible matrix check
  #echo tc.mat
  #echo inverse(tc.mat)
  #quit 2

  var m = Mat4x4[float32](tc.mat)
  var x = inverse(m)
  #echo "herp"
  x

  
proc getProjectionMatrix*(c: Camera): Mat4f = 
  #let ratio = c.width / c.height
  #let ratio = c.aspect
  #ortho[float32](-ratio, ratio, -1.0, 1.0, c.znear, c.zfar)
  #ortho[float32](0.0, 1.0, 0.0, 1.0, c.znear, c.zfar)
  ortho[float32](c.orthoPlanes[0], c.orthoPlanes[1],
    c.orthoPlanes[2], c.orthoPlanes[3], c.znear, c.zfar)
    

proc getWorldMatrix*(c: Camera): Mat4f =
  let tc = c.entity.get(Transform)
  tc.mat

#[
proc project*(c: Camera, original: Vec3f): Vec3f =
  let tc = c.entity.get(Transform)
  #val model = tc.mat
  let model = Mat4(tc.mat) * Mat4(c.getViewMatrix())
  #val model = c.getViewMatrix()
  #val proj = getProjectionMatrix()
  let proj = Mat4(c.getProjectionMatrix()) * Mat4(c.getViewMatrix())
  #let viewport = Vec4f(0.0, 0.0, vars.resX, vars.resY);
  let projected = glm.project(original, model, proj)
  projected
]#
