import ecs, meshbuffer, material, shader
export meshbuffer#, material

var maxMeshId = -1

type
  # Component.
  Mesh* = ref object of Component
    id*: int
    meshName*: string
    material*: Material # nil by default
    mb*: MeshBuffer


proc newMesh*: Mesh =
  maxMeshId += 1
  let m = Mesh(
    id: maxMeshId,
    mb: newMeshBuffer())
  m

proc hasMaterial*(m: Mesh): bool =
  m.material != nil


proc setMaterial*(m: Mesh, mat: Material) =
  m.material = mat


proc loadFromMeshBuffer*(m: Mesh, mb: MeshBuffer) =
  m.mb = mb

