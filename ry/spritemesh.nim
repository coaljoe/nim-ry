import ecs, glm
import node, mesh, card, sceneloader, scene, rendertarget, renderer, spritemeshsys, node, texturesys
import vars as vars
import ry_types
#import globals


# Forward decl.
proc rt(sm: SpriteMesh): RenderTarget
proc update*(sm: SpriteMesh, dt: float)


proc newSpriteMesh*(): SpriteMesh =
  let
    w = vars.resX
    h = vars.resY
    #w = vars.maxSpriteMeshSizeX
    #h = vars.maxSpriteMeshSizeY
  let sm = SpriteMesh(
    #node: newNode(NodeType.Custom),
    #mesh: newMesh(),
    #node: newMeshNode(),
    card: newCard(),
    texR: CreateTexture(w, h, false),
    #rt: newRenderTarget("SpriteMeshRT", w, h, false),
    m_dirty: true)
  sm.card.setDim(0, 0, 0.5, 0.5)
  #sm.card.setDim(0, 0, 1.0, 1.0)
  #sm.card.tex = sm.rt.fb.tex
  #sm.node.addComponent(sm.mesh)
  #sm.node.add(sm.card)
  # Automatically add to the system
  let x = (Node)sm.entity
  #let x = sm.entity
  #gSpriteMeshSys.xadd(x)
  sm


proc newSpriteMeshNode*(): Node =
  let n = newEmptyNode()
  n.nodetype = NodeType.Mesh # fixme??
  let x = newSpriteMesh()
  n.addComponent(x)
  #gSpriteMeshSys.xadd((Node)x.entity)
  n


proc node*(sm: SpriteMesh): Node =
  return (Node)sm.entity


proc rt(sm: SpriteMesh): RenderTarget =
  ## Get sm's render target.
  gSpriteMeshSys.rt


proc load*(sm: SpriteMesh, path: string) =
  let sl = newSceneLoader()
  let newNode = sl.load(path)[0]
  #
  echo newNode
  #quit 2
  #sm.node = newNode
  sm.entity = newNode


#proc spawn*(sm: SpriteMesh, path: string) =
  #ryi.scene.add(sm.node)
  #echo ryi


proc build(sm: SpriteMesh) =
  discard


proc render*(sm: SpriteMesh, r: Renderer) =
  # XXX FIXME
  # call update here since spritemeshsys's update isn't working
  #sm.update(0)

  # Update once
  if not sm.m_dirty:
    return
    
  echo "sm render"
    
  #let w = sm.rt.fb.tex.width
  #let h = sm.rt.fb.tex.height
  let w = sm.rt.fb.width
  let h = sm.rt.fb.height


  sm.rt.bindd()

  r.clear()
  #r.reset()
  #r.renderScene()
  r.renderMesh(sm.node) # Fixme: use renderNode?
  r.finalize()

  # Copy active screen to dst tex
  #CopyTexture(sm.texR.tex, w, h)
  gTextureSys.copyTex(sm.rt.tex, sm.texR.tex)

  sm.rt.unbind()
  
  #r.renderRenderable(asRenderableI(sm.card))
  #sm.card.render(r)
  
  # Update card's tex
  #sm.card.tex = sm.rt.fb.tex
  
  #CopyTexture(sm.texR.tex, w, h)
  sm.card.tex = sm.texR

  # XXX comment out to always update
  #sm.m_dirty = false
  #discard


proc update*(sm: SpriteMesh, dt: float) =
  #echo "sm update"
  let t = sm.entity.get(Transform)
  #echo t.pos, " ", t.rot, " ", t.isDirty(), " ", t.customDirtyFlag
  #if t.isDirty():
  #  sm.m_dirty = true
  if t.customDirtyFlag:
    sm.m_dirty = true
    # Unset flag
    t.customDirtyFlag = false
  #discard

