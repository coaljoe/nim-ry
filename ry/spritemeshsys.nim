import tables, glm
import node, vars, rendertarget#, spritemesh
import ry_types
#import spritemesh
#export gSpriteMeshSys

## XXX: RO?
var gSpriteMeshSys*: SpriteMeshSys

proc initSpriteMeshSys*(): SpriteMeshSys =
  echo "[SpriteMeshSys] new"
  let s = SpriteMeshSys()
  s.elems = newOrderedTable[int, Node]()
  let (w, h) = (vars.resX, vars.resY)
  #let (w, h) = vars.maxSpriteMeshSize
  s.rt = newRenderTarget("SpriteMeshRT", w, h, false)
  echo "s.elems", s.elems
  gSpriteMeshSys = s
  s

proc xadd*(s: SpriteMeshSys, el: Node) =
  s.elems[el.id] = el

proc start*(s: SpriteMeshSys) =
  discard

#proc update*(s: SpriteMeshSys, dt: float) =
#  echo "derp"
#  for el in s.elems.values:
#    let sm = el.get(SpriteMesh)
#    sm.update(dt)