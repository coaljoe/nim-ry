import os, streams, md5, strutils, sequtils, struct, lz4, times

type
  MeshCacheFile* = ref object
    #outfn: string
    #outfs: Filestream
    # Header
    cksum*: string     # Source file checksum
    filepath*: string  # Source file filepath
    lenPositions*: int
    lenNormals*: int
    lenTexcoords*: int
    # Data
    positions*: seq[float32]
    normals*: seq[float32]
    texcoords*: seq[float32]
    positionsRaw*: string
    normalsRaw*: string
    texcoordsRaw*: string

# Forward decl.
#proc init(mcf: MeshCacheFile)
proc show*(mcf: MeshCacheFile)


proc init(mcf: MeshCacheFile) =
  mcf.cksum = ""
  mcf.filepath = ""
  mcf.lenPositions = 0
  mcf.lenNormals = 0
  mcf.lenTexcoords = 0
  mcf.positions = @[]
  mcf.normals = @[]
  mcf.texcoords = @[]
  mcf.positionsRaw = ""
  mcf.normalsRaw = ""
  mcf.texcoordsRaw = ""


proc newMeshCacheFile*(): MeshCacheFile =
  let mcf = MeshCacheFile()
  mcf.init()
  mcf


proc setSourceFile*(mcf: MeshCacheFile, path: string) =
  if not os.existsFile(path):
    quit("path not found; path: " & path)
  
  let f = open(path)
  let fs = newFileStream(f)
  
  mcf.filepath = path
  mcf.cksum = getMd5(fs.readAll())
  f.close()
  


proc setData*(mcf: MeshCacheFile, positions, normals, texcoords: seq[float32]) =
  mcf.positions = positions
  mcf.normals = normals
  mcf.texcoords = texcoords


proc compressData(mcf: MeshCacheFile, d: string): string =
  # lz4 (level=1 is default)
  #compress(d)
  #compress(d, level=2)
  #compress(d, level=9) # XXX Don't use: bad compression ratio
  # lz4hc
  compress_more(d, level=1)
  #compress_more(d, level=9)


proc uncompressData(mcf: MeshCacheFile, d: string): string =
  uncompress(d)


proc write*(mcf: MeshCacheFile, path: string) =
  echo "[MeshCacheFile] write: ", path
  if not os.existsDir(os.splitFile(path)[0]):
    quit("dir not found; path: " & path)
  #if len(mcf.positions) == 0:
  #  quit("positions are empty")
  #if len(mcf.normals) == 0:
  #  quit("normals are empty")
  #if len(mcf.texcoords) == 0:
  #  quit("texcoords are empty")

  let f = open(path, fmWrite)
  #let fs = newFileStream(f)
  #fs.write(mcf.cksum & "\n")
  #fs.write(mcf.filepath & "\n")
  f.write(mcf.cksum & "\L")
  f.write(mcf.filepath & "\L")

  f.write(len(mcf.positions))
  f.write(" ")
  f.write(len(mcf.normals))
  f.write(" ")
  f.write(len(mcf.texcoords))
  f.write("\L")
  f.write("DATA\L")

  var data = ""

  for p in mcf.positions:
    let x = pack("f", p)
    data &= x
    #f.write(mcf.compressData(x))
    #echo "x:", x.unsafeAddr
    #quit(2)
    #f.write(cast[cstring](p))

  for n in mcf.normals:
    let x = pack("f", n)
    data &= x
    #f.write(mcf.compressData(x))

  for tc in mcf.texcoords:
    let x = pack("f", tc)
    data &= x
    #f.write(mcf.compressData(x))

  f.write(mcf.compressData(data))

  f.close()


proc read*(mcf: MeshCacheFile, path: string, onlyRaw: bool = false) =
  echo "[MeshCacheFile] read: ", path
  if not os.existsFile(path):
    quit("file not found; path: " & path)

  let t = cpuTime()

  mcf.init()
  #mcf.show()

  let f = open(path)
  let fs = newFileStream(f)
  mcf.cksum = fs.readLine()
  mcf.filepath = fs.readLine()
  #(mcf.lenPositions, mcf.lenNormals, mcf.lenTexcoords) = lc[parseInt(x) | (x <- splitWhitespace(fs.readLine())), string]
  let ss = splitWhitespace(fs.readLine())
  (mcf.lenPositions, mcf.lenNormals, mcf.lenTexcoords) = map(ss, proc(x: string): int = parseInt(x))
  let dataMagic = fs.readLine()
  if dataMagic != "DATA":
    echo "error: dataMagic is: ", dataMagic
    quit "DATA block not found"

  # Decompress all data
  var data = mcf.uncompressData(fs.readAll())
  let dfs = newStringStream(data)

  # Read raw data
  let sizeGLfloat = 4
  mcf.positionsRaw = dfs.readStr(mcf.lenPositions * sizeGLfloat)
  mcf.normalsRaw = dfs.readStr(mcf.lenNormals * sizeGLfloat)
  mcf.texcoordsRaw = dfs.readStr(mcf.lenTexcoords * sizeGLfloat)
  if not dfs.atEnd():
    echo "not all RAW data have been read; position: ", fs.getPosition(), "path: ", path
    quit "meshcachefile read error"
  dfs.setPosition(0)

  # Read non-raw data
  if not onlyRaw:
    for p in 0..<mcf.lenPositions:
      let x = unpack("f", dfs.readStr(4))[0].getFloat
      mcf.positions.add(x)

    for n in 0..<mcf.lenNormals:
      let x = unpack("f", dfs.readStr(4))[0].getFloat
      mcf.normals.add(x)

    for tc in 0..<mcf.lenTexcoords:
      let x = unpack("f", dfs.readStr(4))[0].getFloat
      mcf.texcoords.add(x)

    if not dfs.atEnd():
      echo "not all data have been read; position: ", fs.getPosition(), "path: ", path
      quit "meshcachefile read error"
  #echo fs.getPosition()
  #echo fs.atEnd()

  f.close()
  
  echo "took: ", cpuTime() - t


proc show*(mcf: MeshCacheFile) =
  echo "--MeshCacheFile:"
  echo "  cksum: ", mcf.cksum
  echo "  filepath: ", mcf.filepath
  echo "  lenPositions: ", mcf.lenPositions
  echo "  lenNormals: ", mcf.lenNormals
  echo "  lenTexcoords: ", mcf.lenTexcoords
  echo "--"