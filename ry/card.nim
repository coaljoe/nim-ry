import ecs
import ry_types
import renderer, texture, draw, vars
export texture

proc newCard*(): Card =
  let c = Card(z: 1, is2d: true,
    showBorder: true)
    #tex: CreateTexture())
  c

proc setDim*(c: Card, x, y, w, h: float) =
  c.x = x
  c.y = y
  c.w = w
  c.h = h

proc render*(c: Card, r: Renderer) =
  #include renderer
  echo "[Card] debug: render"
  
  # XXX fixme
  c.showBorder = vars.debugRender
  
  #quit 2
  if c.tex != nil:
    c.tex.bindd()

  #r.enableBlending()
  if c.is2d:
    r.renderQuad(c.x, c.y, c.w, c.h, c.z)
  else:
    drawPlane(1.0)
  #r.disableBlending()

  if c.tex != nil:
    c.tex.unbind()

  if c.showBorder:
    #drawSetColor(0.5f, 1.0f, 0.5f)
    r.set2DMode()
    drawSetColor(0.6, 0.1, 0.1)
    drawRect(c.x, c.y, c.w, c.h)
    r.unset2DMode()

