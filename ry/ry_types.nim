import collections/iface, glm, opengl, fp/option
import node, scene, framebuffer, shader

# XXX: every field should be public to be accessible,
# because of import

#type
#  Renderable* = concept s
#    xrender(s) is s

#createInterface(RenderPassI):
#  proc name(this: RenderPassI): string
#  #proc render(this: RenderPassI, r: Renderer)
#export RenderPassI

#type
#  RenderPassI = distinct Interface
#
#interfaceMethods RenderPassI:
#  name(): string

type
  RenderPassBase* = ref object of RootObj
    name*: string

  RenderTarget* = ref object
    name*: string
    depth*: bool
    width*: int
    height*: int
    fb*: Framebuffer

  Renderer* = ref object
    #renderables*: seq[Renderable]
    width*: int
    height*: int
    cam*: Node
    scene*: Scene
    renderPasses*: seq[RenderPassBase]
    renderables*: seq[RenderableI]
    trenderer*: TRenderer
    tmpRt*: RenderTarget
    in2dmode*: bool
    m_typ*: RenderType
    # Deferred rendering
    gbuffer*: GBuffer
    geometryShader*: Shader
    lightingShader*: Shader
    showGbuffer*: bool
    debug*: bool

  RenderableI* = distinct Interface    

  RenderType* {.pure.} = enum
    Forward,
    Deferred
    
  GBuffer* = ref object
    # Rendering fbo
    fbo*: Framebuffer
    positionTex*: Texture
    # Diffuse texture
    colorTex*: Texture
    normalTex*: Texture
    texcoordTex*: Texture
    depthTex*: Texture
    textures*: seq[Texture]
  
  SpriteMeshSys* = ref object
    elems*: OrderedTableRef[int, Node]
    rt*: RenderTarget

  # Component.
  SpriteMesh* = ref object of Component
  #SpriteMesh* = ref object of RootObj
    #mesh*: Mesh
    #node*: Node
    card*: Card
    texR*: Texture  # Render texture
    texN*: Texture  # Normal texture
    m_dim*: Vec2f
    #rt: RenderTarget
    m_dirty*: bool

  # Component.
  #Card* = ref object of Component
  Card* = ref object of RootObj
    ## 2d node?
    x*, y*, w*, h*, z*: float
    tex*: Texture
    showBorder*: bool
    is2d*: bool
    #gl
    #fbo*: GLuint
    gb*: GBuffer
    # Depth render buffer object
    rbo_depth*: GLuint

  TextureSys* = ref object
    readFbo*: FrameBuffer
    writeFbo*: FrameBuffer

  Texture* = ref object
    #mipmaps: bool
    #compress: bool
    #anisotropy: bool
    width*: int
    height*: int
    format*: TextureFormat
    minFilter*: TextureFilter
    magFilter*: TextureFilter
    # gl
    tex*: uint32
    gl_min_filter*: int32
    gl_mag_filter*: int32
    
  TextureFormat* {.pure.} = enum
    RGBA,
    RGB16F
    RGB32F

  TextureFilter* {.pure.} = enum
    Nearest
    Linear
  
  TRendererTile* = ref object
    tex*: Texture
    card*: Card
    mesh*: Node
    fbo*: Framebuffer
    gb*: GBuffer
    dirty*: bool

  TRenderer* = ref object of RootObj
    width*: int
    height*: int
    tsize*: int
    tiles*: seq[TRendererTile]
    nodes*: seq[Node]
    rt*: RenderTarget
    tileRt*: RenderTarget
    tcam*: Node
    tcamInitialPos*: Option[Vec3f]
    visible*: bool

  #RenderableI* = tuple[
  #    render: proc(r: Renderer) {.closure.}]


interfaceMethods RenderableI:
  #render*(r: Renderer): T
  render(r: Renderer): RenderableI
  #render(r: Renderer): object
  #render(r: Renderer)
  #RenderPassI* = concept x
  #  #name(x: RenderPassI) is string
  #  #render(x: RenderPassI, r: Renderer) is x
  #  name(x) is string
  #  render(x, Renderer) is x

  #RenderPassI* = tuple[
  #  name: proc(): string {.closure.}]
