import tables, os, system, strutils, typetraits, opengl, glm
import glex, vars

var
  maxShaderId = -1

type
  Shader* = ref object
    id*: int
    #version int
    vs_path*, fs_path*: string
    vs_macro*: string
    fs_macro*: string
    vs_source*: string
    fs_source*: string
    uniform_locs_cache: TableRef[string, int]
    cacheId, cachePath: string
    #shaderHash: uint32
    # gl
    g_shader: GLuint

# Forward decl.
proc load(s: Shader, vs_path, fs_path: string)
proc compile*(s: Shader)
proc saveShaderSource*(s: Shader, fname: string = "dump")
proc `$`*(s: Shader): string

proc newShader*(vs_path, fs_path: string, version: int = 120): Shader =
  #cachePath := vs_path + ";" + fs_path
  maxShaderId += 1
  #version := 130
  #let version = 120
  let s = Shader(
        id: maxShaderId,
        vs_path: vs_path, fs_path: fs_path,
        vs_macro: "#version $1\n".format(version),
        fs_macro: "#version $1\n".format(version),
        uniform_locs_cache: newTable[string, int](),
        #cachePath:           cachePath,
  )
  echo "[Shader] New shader, id=", s.id
  echo s
  echo "vs_path=", s.vs_path
  s.load(vs_path, fs_path)
  s.compile()
  echo "vs_path=", s.vs_path
  s


proc saveShaderSource*(s: Shader, fname: string = "dump") =
  # save shader source
  let exts = ["vert", "frag"]
  for ext in exts:
    var source = ""
    case ext:
    of "vert":
      source = s.vs_macro & s.vs_source
    of "frag":
      source = s.fs_macro & s.fs_source
    else:
      quit("unknown ext: " & ext)
    
    let fout = os.joinPath(os.getTempDir(), fname&"."&ext)

    var f: File
    try:
      f = open(fout, fmWrite)
    except:
      quit("can't save shader to " & fout)
    
    f.writeLine(source)
    f.close()
    echo("shader saved to " & fout)


proc compile*(s: Shader) =
  echo "[Shader] compile"

  let
    vsStr = s.vs_macro & s.vs_source & "\x00"
    fsStr = s.fs_macro & s.fs_source & "\x00"

  let (prg, err) = newProgram(vsStr, fsStr)
  if err:
    quit("shader linker error")
  
  echo "prg: ", prg
  s.g_shader = prg
  echo "xprg: ", s.g_shader
  glcheck()

  #// for rendering
  #s.shaderHash = s.Hash()
  s.saveShaderSource("compile")


proc load(s: Shader, vs_path, fs_path: string) =
  #Log.Inf("loading shaders " + vs_path + " and " + fs_path + "...")
  let vs_path = vars.resPath & vs_path
  let fs_path = vars.resPath & fs_path
  echo("[Shader] loading shaders " & vs_path & " and " & fs_path & "...")

  proc add_includes(dat: string, path: string): string =
    var r = ""
    let lines = dat.splitLines()
    #echo "lines: ", lines
    for l in lines:
      if l == "":
        continue # Skip empty lines
      #echo "l: ", l
      let ss = l.split(" ")
      if ss[0] == "#include":
        var filepath = ss[1].strip() # removes \r on windows
        filepath = filepath.replace("\"", "")
        let fp = os.parentDir(path) & "/" & filepath
        let xdat = system.readFile(fp)
        #Log.Inf("adding shader include from " + fp + "...")
        #if err != nil {
        #  p("file not found", fp)
        #  panic(err)
        #}
        #check(err)
        #echo "filepath: ", filepath
        var txt = "/*#### $1 ####*/\n".format(filepath)
        txt &= add_includes(xdat, fp)
        #txt += add_includes(path)
        txt &= "/*#### end of $1 ####*/\n".format(filepath)
        #println("derp " + filepath)
        #println(txt)
        r &= txt & "\n"
        ##Log.Inf("added shader include from " + conf.res_path + filepath)
        echo("added shader include from " & fp)
      else:
        r &= l & "\n"
    return r

  let
    vsDat = system.readFile(vs_path)
    fsDat = system.readFile(fs_path)

  s.vs_source = add_includes(vsDat, vs_path)
  s.fs_source = add_includes(fsDat, fs_path)

  if false:
    echo "vs_source: ", s.vs_source
    echo "fs_source: ", s.fs_source

  #echo s
  #s.compile()
  #echo s


proc bindd*(s: Shader) =
  #echo "[Shader] bind id: ", s.id, " ", cast[int](s)
  #echo ""
  #echo repr(s)
  #echo "isnil: ", isNil(s)
  #echo s
  #echo s.g_shader
  #echo "isnil s ", isNil(s), " ", s
  #echo isNil(s.vs_path)
  #echo s.vs_path
  #echo "vs_path: ", s.vs_path
  #echo "fs_path: ", s.fs_path
  glUseProgram(s.g_shader)
  #s.passDefaultUniforms()


proc unbind*(s: Shader) =
  #echo "[Shader] unbind"
  glUseProgram(0)
  ##s._shader.Unuse()


proc getUniformLocationCheck*(s: Shader, name: string, check: bool): int =
  ## Can be with or without extra -1 checking.
  # return from cache
  if name in s.uniform_locs_cache:
    return s.uniform_locs_cache[name]
  
  # TODO: -1's are not erros but an optimizations
  # https://www.opengl.org/wiki/GLSL_:_common_mistakes
  glcheck()
  #var xs = (name&"\x00").cstring
  #let loc = cast[int32](glGetUniformLocation(s.g_shader, name))
  var loc = glGetUniformLocation(s.g_shader, name)
  # fixme: doesn't fail? returns -1 instead
  let err = glGetError()
  if err != GL_NO_ERROR:
      echo("err: $1".format(err.cint))
      quit("getUniformLocation failed, name: " & name)
  
  if loc == -1 and check:
    #discard
    quit("shader returned -1 as a uniform location; name: " & name)
    #Log.Err("getUniformLocation returned -1, name: " + name)
    #dump(s)
    #s.saveShaderSource()
    #panic("getUniformLocation returned -1, name: " + name)
  
  # add to cache
  s.uniform_locs_cache[name] = loc
  loc


proc getUniformLocation*(s: Shader, name: string, check = false): int =
  ## Default with no -1 checking.
  s.getUniformLocationCheck(name, check)


proc hasUniform*(s: Shader, name: string): bool =
  ## Check if a uniform was defined.
  s.getUniformLocationCheck(name, false) != -1


proc passUniform3f*(s: Shader, name: string, x, y, z :float32) =
  #s._shader.BindAttribLocation(index, name)
  let loc = s.getUniformLocation(name)
  glUniform3f(GLint(loc), x, y, z)
  let err = glGetError()
  if err != GL_NO_ERROR:
    echo("err: $1".format(err.cint))
    quit("passUniform3f failed, name: " & name)


proc passUniform*[T](s: Shader, name: string, v: T, check = false) =
  #echo "[Shader] passUnitform"
  #defer: echo "[Shader] passUniformDone"
  #s._shader.BindAttribLocation(index, name)
  #quit 2
  let loc = GLint(s.getUniformLocation(name, check = check))
  when T is Mat4f:
    #discard
    var vv = cast[Mat4f](v)
    glUniformMatrix4fv(loc, 1, true, vv.caddr)
  elif T is Vec3f:
    #discard
    #let vv = T
    #glUniform3f(loc, v.x, v.y, v.z)
    let vv = cast[Vec3f](v)
    #let vv = v.Vec3f
    glUniform3f(loc, vv.x, vv.y, vv.z)
    #glUniform3f(loc, v.x, v.y, v.z)
    #discard
    #elif T is Vec2f:
    #  let vv = cast[Vec2f](v)
    #  glUniform2f(loc, vv.x, vv.y)
  elif T is Vec4f:
    let vv = cast[Vec4f](v)
    glUniform4f(loc, vv.x, vv.y, vv.z, vv.w)
  elif T is float:
    let vv = cast[float](v)
    #quit("--> $1".format(vv))
    #let vv = v.float
    glUniform1f(loc, GLfloat(vv))
  elif T is int:
    let vv = cast[int](v)
    #let vv = v.int
    glUniform1i(loc, GLint(vv))
  else:
    quit("unknown T: " & typetraits.name(T))
  let err = glGetError()
  if err != GL_NO_ERROR:
    echo("err: $1".format(err.cint))
    quit("passUniform failed, name: " & name)


proc `$`*(s: Shader): string =
  "Shader<id=$1>".format(s.id)


discard #[
func passUniform2f*(name string, x, y float32) {
    loc := s.GetUniformLocation(name)
    gl.Uniform2f(loc, x, y)
    if err := gl.GetError(); err != gl.NO_ERROR {
        fmt.Println("err:", err)
        panic("PassUniform2f failed, name: " + name)
    }
}

// fixme rename to *V
func (s *Shader) PassUniform3f_(name string, v Vec3) {
    s.PassUniform3f(name, float32(v.X()), float32(v.Y()), float32(v.Z()))
}

func (s *Shader) PassUniform4f(name string, x, y, z, w float64) {
    //s._shader.BindAttribLocation(index, name)
    loc := s.GetUniformLocation(name)
    gl.Uniform4f(loc, float32(x), float32(y), float32(z), float32(w))
    if err := gl.GetError(); err != gl.NO_ERROR {
        fmt.Println("err:", err)
        panic("PassUniform4f failed, name: " + name)
    }
}

// fixme rename to *V
func (s *Shader) PassUniform4f_(name string, v Vec4) {
    s.PassUniform4f(name, v.X(), v.Y(), v.Z(), v.W())
}

func (s *Shader) PassUniform1f(name string, v float64) {
    loc := s.GetUniformLocation(name)
    gl.Uniform1f(loc, float32(v))
    if gl.GetError() != gl.NO_ERROR {
        panic("PassUniform1f failed, name: " + name)
    }
}

func (s *Shader) PassUniform1i(name string, v int) {
    loc := s.GetUniformLocation(name)
    gl.Uniform1i(loc, int32(v))
    if gl.GetError() != gl.NO_ERROR {
        panic("PassUniform1i failed, name: " + name)
    }
}

func (s *Shader) passDefaultUniforms() {
    if s.HasUniform("buffersize") { // fixme: pass always?
        r := rxi.Renderer()
        sx, sy := r.width, r.height
        s.PassUniform2f("buffersize", float32(sx), float32(sy))
    }
    glcheck()
}

func (s *Shader) Free() {
    dbg("Shader.Free")
    gl.DeleteProgram(s._shader)
    //s._shader.Delete()
}
]#

discard #[
func (s *Shader) GetAttribLocation(name string) int32 {
    loc := gl.GetAttribLocation(s._shader, gl.Str(name+"\x00"))
    if loc == -1 {
        println("err: cound not bind attribute; name: ", name)
    }
    if err := gl.GetError(); err != gl.NO_ERROR {
        fmt.Println("err:", err)
        panic("getAttribLocation failed, name: " + name)
    }
    return loc
}
]#
