import opengl
import ry_types
import framebuffer, texture, glex
    
proc newGBuffer*(w, h: int): GBuffer =
  let gb = GBuffer(
    fbo: newFramebuffer(w, h, false),
    colorTex: CreateTexture(w, h,
      filter = [TextureFilter.Nearest, TextureFilter.Nearest]),
    #positionTex: CreateTexture(w, h),
    #normalTex: CreateTexture(w, h),
    positionTex: CreateTexture(w, h, format = TextureFormat.RGB32F,
      filter = [TextureFilter.Nearest, TextureFilter.Nearest]),
    normalTex: CreateTexture(w, h, format = TextureFormat.RGB32F,
      filter = [TextureFilter.Nearest, TextureFilter.Nearest]),
    #positionTex: CreateTexture(w, h, format = TextureFormat.RGB16F),
    #normalTex: CreateTexture(w, h, format = TextureFormat.RGB16F),
    depthTex: CreateTexture(w, h, depth = true),
    textures: @[])
  gb.textures.add(gb.colorTex)
  gb.textures.add(gb.positionTex)
  gb.textures.add(gb.normalTex)
  #gb.textures.add(gb.depthTex)
  gb.fbo.bindd()
  for i, t in gb.textures:
    gb.fbo.attachTexture(t.tex, idx = i)
  #let arr: array[0..2, GLenum] = [GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2]
  #let arr: seq[GLenum] = newSeq()
  var arr = newSeq[GLenum]()
  arr = @[GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2]
  #let drawBuffers = arr
  glDrawBuffers(3, addr arr[0])
  gb.fbo.unbind()
  glcheck()
  gb
  
proc bindForWriting*(gb: GBuffer) =
  ## Bind for geometry pass
  #echo gb == nil
  #echo repr(gb.fbo)
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, gb.fbo.fbo)
  
proc bindForReading*(gb: GBuffer) =
  ## Bind for lighting pass
  glBindFramebuffer(GL_READ_FRAMEBUFFER, gb.fbo.fbo)
  
proc setReadBuffer*(gb: GBuffer, idx: int) =
  glReadBuffer(GLenum(GLint(GL_COLOR_ATTACHMENT0) + GLint(idx)))
  
proc unbind*(gb: GBuffer) =
  ## Unbind all
  glBindFramebufferEXT(GL_FRAMEBUFFER, 0)