var
  resX* = 0
  resY* = 0
  maxSpriteMeshSizeX* = 8 # Read-only
  maxSpriteMeshSizeY* = 8 # Read-only
  maxSpriteMeshSize* = (maxSpriteMeshSizeX, maxSpriteMeshSizeY)
  pw* = 0.0
  ph* = 0.0
  shX* = 0.0
  shY* = 0.0
  debugRender* = false
  # Conf. XXX: fixme?
  #confResPath* = "res"
  resPath* = "res/"
  tmpPath* = "tmp/"
  
