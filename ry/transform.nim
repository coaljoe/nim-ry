import strutils, glm, ecs
import glmex

type
  # component
  Transform* = ref object of Component
    m_mat : Mat4f # model matrix
    pos : Vec3f
    rot : Vec3f
    scale : Vec3f
    dirty : bool
    prevDirty : bool
    customDirtyFlag*: bool

  TransformSys = ref object
    elems: seq[Transform]
  
var
  gTransformSys = TransformSys(elems: @[])

# forward decl.
proc build (t:Transform)

proc newTransform* : Transform =
  let t = Transform(
    m_mat: Mat4Identity,
    pos: Vec3Zero,
    rot: Vec3Zero,
    scale: Vec3One,
    dirty: true,
    prevDirty: false,
    customDirtyFlag: false)
  t.build()
  gTransformSys.elems.add(t)
  t

proc build (t:Transform) =
  #echo "derp"
  let tm = translate(Mat4Identity, t.pos)
  var rm = Mat4Identity
  # yaw, pitch, roll order (Blender: XYZ)
  rm = rotate(rm, vec3f(0, 0, 1), radians(t.rot.z))
  rm = rotate(rm, vec3f(0, 1, 0), radians(t.rot.y))
  rm = rotate(rm, vec3f(1, 0, 0), radians(t.rot.x))
  let sm = scale(Mat4Identity, t.scale)
  #t.mat = tm
  t.m_mat = tm * rm * sm
  #t.mat = sm * rm * tm # wrong
  
  t.dirty = false
  t.customDirtyFlag = true

proc pos* (t:Transform): Vec3f =
  t.pos

proc `pos=`* (t:Transform, v:Vec3f) =
  t.pos = v
  #t.build()
  t.dirty = true

proc rot* (t:Transform): Vec3f =
  t.rot

#proc `rot=`* (t: var Transform, v:Vec3f) =
proc `rot=`* (t:Transform, v:Vec3f) =
  t.rot = v
  #t.build()
  t.dirty = true
  
proc scale*(t: Transform): Vec3f =
  t.scale

proc `scale=`*(t: Transform, v: Vec3f) =
  t.scale = v
  #t.build()
  t.dirty = true

proc `scale=`*(t: Transform, v: float) =
  t.scale = vec3f(v)
  t.dirty = true

proc mat* (t:Transform): Mat4f =
  if t.dirty:
    t.build()
  t.m_mat

proc mat3(t: Transform): Mat3f =
  let mat = t.mat()
  mat3f(mat[0].xyz, mat[1].xyz, mat[2].xyz)

proc `mat=`* (t:Transform, v: Mat4f) =
  t.m_mat = v
  quit("not implemented")
#  decompose mat?
#  t.build()

proc `dirty=`(t: Transform, v: bool) =
  t.prevDirty = t.dirty
  t.dirty = v

proc isDirty*(t: Transform): bool =
  return t.dirty
  
proc isPrevDirty*(t: Transform): bool =
  t.prevDirty

proc forceBuild*(t: Transform) =
  t.build()

proc translate*(t: Transform, v: Vec3f) =
  t.pos = t.pos + v
  t.dirty = true

proc translateLocal*(t: Transform, v: Vec3f) =
  ## Move by vec (locally)

  #let matt = mat3(t.mat) # world(?) rotation matrix
  # world(?) rotation matrix
  #let mat = t.mat()
  #let matt = mat3f(mat[0].xyz, mat[1].xyz, mat[2].xyz)
  let matt = t.mat3()
  #mat.SetTranslation(0, 0, 0) // clear translation

  let trans_local = v
  #let trans_world = matt * trans_local
  let trans_world = matt * trans_local
  let new_pos = t.pos + trans_world

  t.pos = new_pos
  #echo t.pos, " ", new_pos
  #t.build()
  t.dirty = true

proc lookAt*(t: Transform, v: Vec3f, up: Vec3f = vec3f(0, 0, 1)) =
  let center = t.pos
  let eye = v
  #let center = v
  #let eye = t.pos
  #let up = vec3f(0, 0, 1)
  t.m_mat = lookAt(center, eye, up)
  t.dirty = false

proc billboard*(t: Transform, cameraPos: Vec3f) =
  let cameraUp = vec3f(0, 0, 1)
  let position = t.pos

  let look = normalize(cameraPos - position)
  let right = cross(cameraUp, look)
  let up2 = cross(look, right)
  var transform = mat4f(1.0)
  transform[0] = vec4f(right, 0)
  transform[1] = vec4f(up2, 0)
  transform[2] = vec4f(look, 0)
  # Uncomment this line to translate the position as well
  # (without it, it's just a rotation)
  #transform[3] = vec4(position, 0);
  #transform[3] = vec4f(position, 0)
  let sm = scale(Mat4Identity, t.scale)
  transform = transform * sm
  echo transform
  t.m_mat = transform
  t.dirty = false

proc `$`*(t: Transform): string =
  "Transform<pos: $1, rot: $2, scale: $3>".format(t.pos, t.rot, t.scale)

const moveLocal* = translateLocal
  
proc update*(t: Transform, dt: float) =
  discard

###############
# TransformSys
  
proc update(s: TransformSys, dt: float) =
  for el in s.elems:
    el.update(dt)
