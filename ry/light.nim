import glm, ecs
import glmex

# Component.
type
  Light* = ref object of Component
    # Intensities
    color*: Vec3f
    #intensity*: float


proc newLight* : Light =
  let li = Light(color: Vec3One)#, intensity: 1.0)
  li
