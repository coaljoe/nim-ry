import os, streams, strutils, md5, opengl, nimasset/obj, ecs
#import nimasset/collada
import node, nodeex, mesh, meshbuffer, meshcachefile, vars

type
  SceneLoader* = ref object
    #loader: ColladaLoader
    loader: ObjLoader
    nodes: seq[Node]


proc newSceneLoader*: SceneLoader =
  #result = SceneLoader(loader: ColladaLoader.new)
  #result = SceneLoader(loader: ObjLoader.new, nodes: @[])
  result = SceneLoader(loader: ObjLoader.new, nodes: newSeq[Node](0))


proc load*(sl: SceneLoader, path: string): seq[Node] =
  ## XXX Warning: this file is using patched version of nimasset,
  ## patched lines: obj.nim:31;
  ## Unrolling code from here:
  ## https://github.com/opengl-tutorials/ogl/blob/master/common/objloader.cpp
  
  let xpath = vars.resPath & path
  echo "[SceneLoader] loading path: ", xpath

  if not os.existsFile(xpath):
    quit("path not found; path: " & xpath)
  
  let
    f = open(xpath)
    fs = newFileStream(f)
  defer: f.close()

  if false:
    echo "file content:"
    echo fs.readAll()
    fs.setPosition(0)
    #quit(2)

  # Prepare cache
  let cacheDir = vars.tmpPath & "cache" #"tmp/cache"
  var cacheFile = ""
  var loadFromCacheFile = false
  if not os.existsDir(cacheDir):
    os.createDir(cacheDir)

  # Prepare meshcache file
  let mcf = newMeshCacheFile()
  mcf.setSourceFile(xpath)
  cacheFile = cacheDir & "/" & "mesh-" & mcf.cksum
  #quit cacheFile

  if os.fileExists(cacheFile):
    # Calculate current file cksum
    let fileCkSum = getMd5(fs.readAll())
    #mcf.read(cacheFile, onlyRaw=false)
    mcf.read(cacheFile, onlyRaw=true)
    if fileCkSum == mcf.cksum:
      loadFromCacheFile = true
    fs.setPosition(0) # Return file seek

  var
    positions: seq[float32] = @[] # Temp
    normals: seq[float32] = @[]   # Temp
    texcoords: seq[float32] = @[] # Temp
    vertexIndices: seq[int] = @[]
    uvIndices: seq[int] = @[]
    normalIndices: seq[int] = @[]
    out_positions: seq[float32] = @[]
    out_texcoords: seq[float32] = @[]
    out_normals: seq[float32] = @[]

  proc addVertex(x, y, z: float) =
    echo "Vertex: ", x, " ", y, " ", z
    #positions.add(@[x, y, z])
    positions.add(@[x.float32, y.float32, z.float32])

  proc addTexture(u, v, w: float) =
    echo "Texture: ", u, " ", v, " ", w
    #texcoords.add(@[u, v, w])
    #texcoords.add(@[u.GLfloat, v.GLfloat, w.GLfloat])
    texcoords.add(@[u.float32, v.float32])
    
  proc addNormal(x, y, z: float) =
    echo "Normal: ", x, " ", y, " ", z
    normals.add(@[x.float32, y.float32, z.float32])

  proc addFace(vi0, vi1, vi2, ti0, ti1, ti2, ni0, ni1, ni2: int) =
    echo "Face: ", vi0, " ", vi1, " ", vi2, " ", ti0, " ", ti1, " ", ti2, " ", ni0, " ", ni1, " ", ni2
    #indices.add(@[vi0.uint32, vi1.uint32, vi2.uint32])
    #indices.add(@[vi0.uint32, vi1.uint32, vi2.uint32, ti0.uint32, ti1.uint32])
    #indices.add(@[uint32(vi0-1), uint32(vi1-1), uint32(vi2-1), 0, 0])
    vertexIndices.add(@[vi0, vi1, vi2])
    uvIndices.add(@[ti0, ti1, ti2])
    normalIndices.add(@[ni0, ni1, ni2])

  if not loadFromCacheFile:
    # Loading mesh data
    loadMeshData(sl.loader, fs, addVertex, addTexture, addNormal, addFace)

    for i in 0..<vertexIndices.len:
      let vi = (vertexIndices[i]-1) * 3
      let uvi = (uvIndices[i]-1) * 2
      let ni = (normalIndices[i]-1) * 3
      let (pos_x, pos_y, pos_z) = (positions[vi], positions[vi+1], positions[vi+2])
      let (tex_u, tex_v, tex_w) = (texcoords[uvi], texcoords[uvi+1], 0.0)
      let (nor_x, nor_y, nor_z) = (positions[vi], positions[vi+1], positions[vi+2])
      out_positions.add(@[pos_x, pos_y, pos_z])
      out_texcoords.add(@[tex_u, tex_v])
      out_normals.add(@[nor_x, nor_y, nor_z])
  else:
    # Loading data from cache file
    out_positions = mcf.positions
    out_normals = mcf.normals
    out_texcoords = mcf.texcoords

  let n = newMeshNode()
  let m = n.get(Mesh)
  let mb = newMeshBuffer()
  #mb.positionsRaw = mcf.positionsRaw
  if false:
    # Create mesh buffer from parsed data
    # Note: onlyRaw MCF reading should be disabled
    mb.add(out_positions, out_normals, out_texcoords, @[])
  else:
    # Create meshbuffer from raw data
    mb.addRawData(mcf.positionsRaw, mcf.normalsRaw, mcf.texcoordsRaw)
  m.loadFromMeshBuffer(mb)

  sl.nodes.add(n)

  # Save a meshcache file
  if not loadFromCacheFile:
    #echo len(out_normals)
    mcf.setData(out_positions, out_normals, out_texcoords)
    #mcf.write(cacheDir & "/" & mcf.cksum)
    mcf.write(cacheFile)

  if false:
    mcf.read(cacheDir & "/" & mcf.cksum)
    mcf.show()
    echo mcf.positions
    echo mcf.normals
    echo mcf.texcoords
    quit 2

  echo "[SceneLoader] loading done"
  return sl.nodes
  

#[
proc load* (sl:SceneLoader, path:string): seq[Entity] =
  let
    f = open(path)
    fs = newFileStream(f)

  #let s = fs.readAll()
  #fs.writeLine(s)
  let scene = sl.loader.load(fs)

  echo scene
  echo scene.rootNode.geometry
  echo scene.path
  #echo scene.childNodesGeometry[0].name
  echo "derp"

  for child in scene.rootNode.children:
      echo child.name

  result = @[]
]#
