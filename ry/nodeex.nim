import ecs
import ry_types
import mesh, light, camera
import node#, spritemesh


proc newMeshNode*(): Node =
  let n = newEmptyNode()
  n.nodetype = NodeType.Mesh # fixme
  n.addComponent(newMesh())
  n


proc newLightNode*(name: string = ""): Node =
  let n = newEmptyNode()
  n.nodetype = NodeType.Light # fixme
  n.addComponent(newLight())
  if n.name != "":
    n.name = name
  n
  

proc newCameraNode*(name: string = ""): Node =
  let n = newEmptyNode()
  n.nodetype = NodeType.Camera # fixme
  n.addComponent(newCamera())
  if n.name != "":
    n.name = name
  n

#[
proc newSpriteMeshNode*(): Node =
  let n = newEmptyNode()
  n.nodetype = NodeType.Mesh # fixme??
  let x = newSpriteMesh()
  n.addComponent(x)
  #gSpriteMeshSys.xadd((Node)x.entity)
  n
]#
