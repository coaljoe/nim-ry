import glm, tables
import node, vars
export tables

type
  Scene* = ref object
    nodes: OrderedTableRef[int, Node]  # Readonly?
    camera*: Node  # Active camera (per scene)
    light0*: Node  # Default light (fixme)

var gScene*: Scene

# Readonly.
proc nodes*(s:Scene): OrderedTableRef[int, Node] = s.nodes


proc newScene*: Scene =
  let s = Scene()
  s.nodes = newOrderedTable[int, Node]()
  echo "s.nodes", s.nodes
  gScene = s
  s


proc getNodes*(s: Scene): seq[Node] =
  result = @[]
  for v in s.nodes.values:
    result.add(v)


proc getNodeById*(s: Scene, id: int): Node =
  for n in s.nodes.values:
    if n.id == id:
      return n
  quit("node not found; id: ", id)


proc getNodesWithComponent*[T](s: Scene, c: typedesc[T]): seq[Node] =
  result = @[]
  for n in s.nodes.values:
    if n.hasComponent(c):
      result.add(n)


proc hasNode*(s: Scene, n: Node): bool =
  s.nodes.hasKey(n.id)


proc addNode*(s: Scene, n: Node) =
  if n == nil:
    quit "can't add nil node"
  if s.hasNode(n):
    quit "alredy have this node;" & $n
  s.nodes[n.id] = n


proc addNodes*(s: Scene, ns: seq[Node]) =
  for n in ns:
    s.addNode(n)


proc removeNode*(s: Scene, n: Node) =
  if not s.hasNode(n):
    quit "node not found"
  del(s.nodes, n.id)
