import times, opengl, glfw, glfw/wrapper, glm, ecs
import ry_types, globals
import scene, transform, camera, vars

## Window
#####

type
  Window* = ref object
    glfwWin: glfw.Win

# Forward decl.
#proc keyCb(w: Window, win: Win, key: Key, scanCode: int, action: KeyAction,
#           modKeys: ModifierKeySet) {.procvar.}


proc newWindow*(): Window =
  let w = Window()
  return w


proc init(w: Window, width: int, height: int) =
  glfw.init()

  windowHint(CONTEXT_VERSION_MAJOR, 2)
  windowHint(CONTEXT_VERSION_MINOR, 1)

  #glfw.windowHint(GLFW_WINDOW_NO_RESIZE, true)
  #glfw.windowHint(glfw.FSAA_SAMPLES, 8)
  #glfw.windowHint(glfw.Samples, 0)
  #glfw.windowHint(glfw.Samples, 2)
  #glfw.windowHint(glfw.Samples, 8)
  #windowHint(SAMPLES, GLFW_DONT_CARE) # Use os settings
  #windowHint(SAMPLES, 0)
  windowHint(RESIZABLE.cint, GL_FALSE.cint)
  windowHint(FOCUSED.cint, GL_TRUE.cint)

  windowHint(RED_BITS, 8)
  windowHint(GREEN_BITS, 8)
  windowHint(BLUE_BITS, 8)
  windowHint(ALPHA_BITS, 0)
  windowHint(DEPTH_BITS, 24)
  windowHint(STENCIL_BITS, 0)

  w.glfwWin = newGlWin(
    dim = (w: width, h: height),
    title = "ry",
    resizable = false,
    version = glv21
  )
  #w.glfwWin = win
  let win = w.glfwWin

  # Fixme: move somewhere else
  proc keyCb(win: Win, key: Key, scanCode: int, action: KeyAction,
             modKeys: ModifierKeySet) {.procvar.} =

    if key in @[keyEscape, keyQ] and action == kaDown:
      win.shouldClose = true

  win.keyCb = keyCb

  glfw.makeContextCurrent(w.glfwWin)

  #if not gladLoadGL(getProcAddress):
  #  quit "Error initialising OpenGL"

  glfw.swapInterval(1)

  #init()
  
  # init opengl
  loadExtensions()

  if extensionSupported("GL_EXT_framebuffer_blit") == 0:
    quit "ext not surrorted"


proc isKeyDown*(w: Window, key: glfw.Key): bool =
  glfw.isKeyDown(w.glfwWin, key)


proc hasExit*(w: Window): bool =
  w.glfwWin.shouldClose


proc update(w: Window, dt: float) =
  glfw.swapBufs(w.glfwWin)
  glfw.pollEvents()


## App
######

type
  App* = ref object
    win*: Window
    timeLast: float
    dt*: float
    frame*: int
    #ryi: Ry


proc newApp*(): App =
  let app = App(
    win: newWindow(),
    timeLast: 0.0,
    #ryi: ryi,
  )
  app


proc init*(a: App, width: int, height: int) =
  a.win.init(width, height)


proc quit*(a: App) =
  echo "app.quit"


proc update(a: App, dt: float) =
  # Pan camera
  let cameraNode = gScene.camera
  var adjX = 0.0
  var adjY = 0.0
  var speed = 5.0
  var cameraMoved = false
  if glfw.isKeyDown(a.win.glfwWin, glfw.keyLeftShift) or
    glfw.isKeyDown(a.win.glfwWin, glfw.keyRightShift):
      speed *= 4
      cameraMoved = true
  
  if glfw.isKeyDown(a.win.glfwWin, glfw.keyLeft):
      adjX = -speed
      cameraMoved = true
  elif glfw.isKeyDown(a.win.glfwWin, glfw.keyRight):
      adjX = speed
      cameraMoved = true
  
  if glfw.isKeyDown(a.win.glfwWin, glfw.keyUp):
      adjY = speed
      cameraMoved = true
  elif glfw.isKeyDown(a.win.glfwWin, glfw.keyDown):
      adjY = -speed
      cameraMoved = true
  
  #let cam = cameraNode.get(Camera)
  #cam.unproject()
  #cameraNode.transform.pos += Vec3(adjX * dt, adjY * dt, 0)
  let tc = cameraNode.get(Transform)
  tc.translateLocal(vec3f(adjX * dt, adjY * dt, 0))
  tc.forceBuild()
  let c_fov = 45.0
  let ppu = float(vars.resX) / c_fov
  #let shX = (adjX * dt) * ppu
  #let shX = tc.pos.x * ppu
  #let shX = (tc.pos.x * cos(tc.rot.x)) * ppu
  #let vec = vec3f(tc.pos.x, 0, 0)
  #let vec = tc.pos
  #let normal = vec3f(1, 0, 0)
  #let shX = (dot(vec, normal) / dot(normal, normal) * normal).x * ppu
  #let shX = (length(tc.pos) * cos(tc.rot)).x * ppu
  #let shX = (tc.pos.x - cos(tc.rot.x)) * ppu
  #let q = radians(tc.rot.y)
  #let shX = (tc.pos.x * cos(q) - tc.pos.y * sin(q)) * ppu
  let q = vec4f(tc.pos.x, tc.pos.y, tc.pos.z, 1.0)
  let shX = (q * tc.mat).x * ppu
  let shY = (q * tc.mat).y * ppu
  vars.shX = shX
  vars.shY = shY
  #echo "shX: ", shX
  #echo "shY: ", shY

  if cameraMoved:
    #discard
    echo "camera moved ", adjX*dt, " ", adjY*dt
    echo "shX: ", shX
    #quit 2
    #for (sm in SpriteMeshSys.elems.toTypedArray()) {
    #    sm.onCameraMove(adjX, adjY)
    #}

  # Fixme
  if a.win.isKeyDown(glfw.keyF1):
    gRenderer.showGbuffer = not gRenderer.showGbuffer
  if a.win.isKeyDown(glfw.keyF10):
    vars.debugRender = not vars.debugRender
  
  a.win.update(dt)


proc step*(a: App): bool =
  if a.win.hasExit():
    a.quit()
    return false

  let timeNow = epochTime()

  # first frame
  if a.timeLast == 0:
    a.timeLast = timeNow
  
  a.dt = timeNow - a.timeLast
  #echo timeNow, " ", a.timeLast, " ", a.dt
  a.timeLast = timeNow
  
  #ryi().render()

  a.update(a.dt)
  a.frame += 1
  return true

