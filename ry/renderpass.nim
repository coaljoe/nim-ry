import rendertarget, texture
import ry_types, vars as vars

#type RenderPassI interface {
# Name() string
# Render(r *Renderer)
#}

type
  #RenderPassI* = concept x
  #  name(x) is string
  #  #render(x, r: Renderer) is x

  RenderPass* = ref object of RenderPassBase
    #name*: string
    #inTex: Texture # fboTex from previous renderpass
    #outTex: Texture # fboTex
    inTex*: uint32 # fboTex from previous renderpass
    outTex*: uint32 # fboTex
    prevRp*: RenderPass
    fboRt*: RenderTarget # RP's default render target

#method name*(rp: RenderPass): string = rp.RenderPassBase.name


## RenderPassBase
####

method name*(rpb: RenderPassBase): string {.base.} =
  rpb.name


method setName*(rpb: RenderPassBase, name: string) {.base.} =
  rpb.name = name


method render*(rpb: RenderPassBase, r: Renderer) {.base.} =
  quit "override RenderPassBase.Render"


## RenderPass
####

proc newRenderPass*(name: string): RenderPass =
  let rp = RenderPass(
    fboRt:  newRenderTarget(name&"_rt", vars.resX, vars.resY, false),
    prevRp: nil)
  #rp.RenderPassBase.name = name
  #rp.setName(name)
  rp.name = name
  rp
  #[ 
    // init
    if rp.prevRp != nil {
      rp.inTex = rp.prevRp.InTex()
    }
  ]#


proc tex*(rp: RenderPass): uint32 =
  ## Shortcut to fbo tex, same as OutTex.
  rp.fboRt.tex


proc inTex*(rp: RenderPass): uint32 =
  ## Get previous RP's texture.
  if rp.prevRp == nil:
    quit("no prev render pass")
  
  rp.prevRp.tex


proc setPrevRenderPass*(rp: RenderPass, prevRp: RenderPass) =
  ## Set or unset previous render pass (nil to unset).
  rp.prevRp = prevRp


proc hasPrevRenderPass*(rp: RenderPass): bool =
  rp.prevRp != nil


method render*(rp: RenderPass, r: Renderer) =
  echo "RenderPass.Render"
