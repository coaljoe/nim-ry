# Package

version       = "0.1.0"
author        = "coaljoe"
description   = "ry"
license       = "MIT"
#bin           = @["main_ry"]
#srcDir        = "src"

# Dependencies

# ecs, nimasset
requires @["nim >= 0.14.2", "glm", "opengl", "nim-glfw", "collections", "stb_image >= 1.2", "struct",
  "nimlz4", "nimfp", "ecs", "nimasset"]

