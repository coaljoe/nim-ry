import types
#proc withB*(a: A, b: B)

import ma

proc `$`*(b: B): string =
  "obj B"
  
proc withA*(b: B, a: A) =
  echo "do something with A"
  #a.withB(b)
  a.xwithB(b)
  
proc xwithA*(b: B, a: A) =
  echo "xwith A"