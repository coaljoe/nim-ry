import types
proc xwithB*(a: A, b: B)
import mb

proc `$`*(a: A): string =
  "obj A"
  
proc withB*(a: A, b: B) =
  echo "do something with B"
  #b.withA(a)
  b.xwithA(a)
  
proc xwithB*(a: A, b: B) =
  echo "xwith B"
