import ma
import mb
import types

proc main() =
  echo "main()"
  let a = A()
  let b = B()
  echo a, " ", b
  a.withB(b)
  b.withA(a)
  echo "done"
  
main()